package com.brainstrom.onestore;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.brainstrom.onestore.adapter.ListViewAdapterForShoppingList;
import com.brainstrom.onestore.database.ShoppingListDatabaseHandler;
import com.brainstrom.onestore.database.ShoppingListFrame;
import com.daimajia.swipe.implments.SwipeItemMangerImpl;

public class ShoppingList extends Activity {

	private ListView mListView;
	private ListViewAdapterForShoppingList mAdapter;
	private Context mContext = this;
	Button bPublish;
	String _baseurl, username;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_shopping_list);
        getActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.color_actionbar)));
		mListView = (ListView) findViewById(R.id.lvShoppingList);
		bPublish = (Button) findViewById(R.id.bShoppingListPublish);

		try {
			ShoppingListDatabaseHandler db = new ShoppingListDatabaseHandler(
					getApplicationContext());
			int count = db.getshoppingListsCount();
			if (count == 0) {
				bPublish.setVisibility(View.GONE);
			}
		} catch (Exception e) {

		}

		bPublish.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				toast("Publishing...");
				loadToCallPublishList();
			}
		});

		/*
		 * The following comment is the sample usage of ArraySwipeAdapter.
		 */
		// String[] adapterData = new String[]{"Activity", "Service",
		// "Content Provider", "Intent", "BroadcastReceiver", "ADT", "Sqlite3",
		// "HttpClient",
		// "DDMS", "Android Studio", "Fragment", "Loader", "Activity",
		// "Service", "Content Provider", "Intent",
		// "BroadcastReceiver", "ADT", "Sqlite3", "HttpClient", "Activity",
		// "Service", "Content Provider", "Intent",
		// "BroadcastReceiver", "ADT", "Sqlite3", "HttpClient"};
		// mListView.setAdapter(new ArraySwipeAdapterSample<String>(this,
		// R.layout.listview_item, R.id.position, adapterData));

		mAdapter = new ListViewAdapterForShoppingList(this);
		mListView.setAdapter(mAdapter);
		mAdapter.setMode(SwipeItemMangerImpl.Mode.Single);
		mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Toast.makeText(mContext, "Double Tap To View, Slide to Edit",
						Toast.LENGTH_SHORT).show();
			}
		});
	}

	private void getBaseUrl() {
		SharedPreferences pref = getApplicationContext().getSharedPreferences(
				"oneStorePref", 0);
		_baseurl = pref.getString("base_url",
				getResources().getString(R.string.base_url));
		username = pref.getString("username", "");
	}

	private void getData(String list) {
		getBaseUrl();
		String url = _baseurl
				+ "/lists/lists.insert.php?apikey=tejpratap&username="
				+ username + "&items=" + list;
		// Instantiate the RequestQueue.
		RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

		// Request a string response from the provided URL.
		StringRequest stringRequest = new StringRequest(Request.Method.GET,
				url, new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						// TODO Auto-generated method stub
						publishList(response);
					}

				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						toast("No internet connection");
					}
				});
		// Add the request to the RequestQueue.
		queue.add(stringRequest);
	}

	private void publishList(String response) {
		// TODO Auto-generated method stub
		try {
			JSONObject main = new JSONObject(response);
			int status = main.getInt("status");
			if (status == 1) {
				toast("List Published, see it in your lists now");
				deleteAll();
				mAdapter = new ListViewAdapterForShoppingList(this);
				mListView.setAdapter(mAdapter);
				mAdapter.setMode(SwipeItemMangerImpl.Mode.Single);
			} else {
				toast("Error " + main.getString("error"));
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void loadToCallPublishList() {
		ShoppingListDatabaseHandler db = new ShoppingListDatabaseHandler(
				mContext);
		List<ShoppingListFrame> shoppinglist = db.getAllshoppingLists();
		String list = "";
		for (ShoppingListFrame cn : shoppinglist) {
			String log = "Id: " + cn.getID() + " ,Name: " + cn.getName()
					+ " ,price: " + cn.getPrice() + " ,quantity: "
					+ cn.getQuantity();
			// Writing Contacts to log
			Log.d("Name: ", log);
			list += "<id>" + cn.getID() + "</id><name>" + cn.getName()
					+ "</name><quantity>" + cn.getQuantity()
					+ "</quantity><cost>" + cn.getPrice().replace("Rs. ","") + "</cost>";
		}
		getData(list.replace(" ", "+"));
	}

	private void deleteAll() {
		ShoppingListDatabaseHandler db = new ShoppingListDatabaseHandler(
				getApplicationContext());
		db.deleteAll();
	}

	private void toast(String str) {
		Toast.makeText(getApplicationContext(), str, Toast.LENGTH_LONG).show();
	}

	// @Override
	// public boolean onCreateOptionsMenu(Menu menu) {
	// // Inflate the menu; this adds items to the action bar if it is present.
	// getMenuInflater().inflate(R.menu.main, menu);
	// return true;
	// }
	//
	// @Override
	// public boolean onOptionsItemSelected(MenuItem item) {
	// // Handle action bar item clicks here. The action bar will
	// // automatically handle clicks on the Home/Up button, so long
	// // as you specify a parent activity in AndroidManifest.xml.
	// int id = item.getItemId();
	// return super.onOptionsItemSelected(item);
	// }
}
