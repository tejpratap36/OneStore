package com.brainstrom.onestore;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import net.sourceforge.zbar.Symbol;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.brainstrom.onestore.adapter.MenuListAdapter;
import com.brainstrom.onestore.extras.Contents;
import com.brainstrom.onestore.extras.QRCodeEncoder;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;

@SuppressWarnings("deprecation")
public class Home extends FragmentActivity implements ActionBar.TabListener,
		OnClickListener, OnItemClickListener {

    GoogleCloudMessaging gcm;
    String regid;
    String PROJECT_NUMBER = "270571231689";

	// drawer items
	LinearLayout llLogin, llAfterLogin, llManage;
	ImageButton ibShoppingList;
	Button bRegister, bLogin, bProfile;
	ListView lvMenu;
	TextView tvUserDetails;
	ArrayList<String> ListName = new ArrayList<String>();
	ArrayList<String> ListSub = new ArrayList<String>();
	ArrayList<String> ListSign = new ArrayList<String>();
	DrawerLayout mDrawerLayout;
	ActionBarDrawerToggle mDrawerToggle;
	LinearLayout llDrawer;

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		// Set up the action bar.
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						actionBar.setSelectedNavigationItem(position);
					}
				});

		// For each of the sections in the app, add a tab to the action bar.
		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			// Create a tab with text corresponding to the page title defined by
			// the adapter. Also specify this Activity object, which implements
			// the TabListener interface, as the callback (listener) for when
			// this tab is selected.
			actionBar.addTab(actionBar.newTab()
					.setText(mSectionsPagerAdapter.getPageTitle(i))
					.setTabListener(this)
					.setIcon(mSectionsPagerAdapter.getPageIcon(i)));

			// .setText(mSectionsPagerAdapter.getPageTitle(i))
		}

		ActionBar ab = getActionBar();
		ab.setTitle("One Store");
		ab.setBackgroundDrawable(new ColorDrawable(getResources().getColor(
				R.color.color_actionbar)));
		setNavigationDrawer();

		// drawer items init
		llLogin = (LinearLayout) findViewById(R.id.llHomeLogin);
		llAfterLogin = (LinearLayout) findViewById(R.id.llHomeAfterLogin);
		llManage = (LinearLayout) findViewById(R.id.llHomeManage);
		ibShoppingList = (ImageButton) findViewById(R.id.ibList);
		lvMenu = (ListView) findViewById(R.id.lvHomeMenu);
		bLogin = (Button) findViewById(R.id.bHomeLogin);
		bRegister = (Button) findViewById(R.id.bHomeRegister);
		bProfile = (Button) findViewById(R.id.bHomeViewProfile);
		tvUserDetails = (TextView) findViewById(R.id.tvHomeUserData);
		ibShoppingList.setOnClickListener(this);
		clearList();
		setList();
		lvMenu.setAdapter(new MenuListAdapter(getApplicationContext(),
				ListName, ListSub, ListSign));
		lvMenu.setOnItemClickListener(this);
		bLogin.setOnClickListener(this);
		bRegister.setOnClickListener(this);
		bProfile.setOnClickListener(this);

        getRegId();
	}

    public void getRegId() {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if(gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                    }

                    regid = gcm.register(PROJECT_NUMBER);
                    msg = "Device registered, registration ID=" + regid;
                    Log.i("GCM", "!!!!! " + regid);

                } catch(IOException ex) {
                    msg = "Error: " + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                Log.d("GCM ID",msg);
            }
        }.execute(null, null, null);
    }

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// If the nav drawer is open, hide action items related to the content
		// view
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(llDrawer);
		menu.findItem(R.id.menuSettings).setVisible(!drawerOpen);
		menu.findItem(R.id.menuViewCart).setVisible(!drawerOpen);
		menu.findItem(R.id.menuSearch).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.menuSettings:
			Intent ms = new Intent(getApplicationContext(), MarketShort.class);
			startActivity(ms);
			break;
		case R.id.menuViewCart:
			Intent sl = new Intent(getApplicationContext(), ShoppingList.class);
			startActivity(sl);
			break;
		case R.id.menuSearch:
			Intent se = new Intent(getApplicationContext(), Search.class);
			startActivity(se);
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
		// When the given tab is selected, switch to the corresponding page in
		// the ViewPager.
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a DummySectionFragment (defined as a static inner class
			// below) with the page number as its lone argument.
			Fragment fragment = null;
			switch (position) {
			case 0:
				fragment = new FeaturedSectionFragment();
				break;
			case 1:
				fragment = new NewArrivalSectionFragment();
				break;
			case 2:
				fragment = new TopSectionFragment();
				break;
			case 3:
				fragment = new MenuSectionFragment();
				break;
			default:
				fragment = new FeaturedSectionFragment();
				break;
			}
			return fragment;
		}

		@Override
		public int getCount() {
			// Show 3 total pages.
			return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return "Featured".toUpperCase(l);
			case 1:
				return "New Arrival".toUpperCase(l);
			case 2:
				return "Top Selling".toUpperCase(l);
			case 3:
				return "Menu".toUpperCase(l);
			}
			return null;
		}

		private int getPageIcon(int position) {
			switch (position) {
			case 0:
				return R.drawable.featured;
			case 1:
				return R.drawable.fresh;
			case 2:
				return R.drawable.top;
			case 3:
				return R.drawable.featured;
			}
			return R.drawable.featured;
		}
	}

	// drawer layout

	private void setNavigationDrawer() {
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		llDrawer = (LinearLayout) findViewById(R.id.lldrawer);
		ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this,
				mDrawerLayout, R.drawable.ic_drawer, R.string.drawer_open,
				R.string.drawer_close) {

			/** Called when a drawer has settled in a completely closed state. */
			public void onDrawerClosed(View view) {
				super.onDrawerClosed(view);
				getActionBar().setTitle("One Store");
				invalidateOptionsMenu();
			}

			/** Called when a drawer has settled in a completely open state. */
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				getActionBar().setTitle("Navigate");
				invalidateOptionsMenu();
			}
		};

		// Set the drawer toggle as the DrawerListener
		mDrawerLayout.setDrawerListener(mDrawerToggle);
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		SharedPreferences pref = getApplicationContext().getSharedPreferences(
				"oneStorePref", 0);
		if (pref.getString("username", "").length() > 0) {
			llAfterLogin.setVisibility(View.VISIBLE);
			llManage.setVisibility(View.VISIBLE);
			llLogin.setVisibility(View.GONE);
			tvUserDetails.setText("Welcome " + pref.getString("username", ""));
		} else {
			llLogin.setVisibility(View.VISIBLE);
			llAfterLogin.setVisibility(View.GONE);
			llManage.setVisibility(View.GONE);
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (llDrawer.getVisibility() == View.VISIBLE) {
			mDrawerLayout.closeDrawers();
		} else {
			super.onBackPressed();
		}
	}

	private void clearList() {
		ListSub.clear();
		ListName.clear();
		ListSign.clear();
	}

	private void setList() {
		ListName.add("View Profile");
		ListSub.add("View your profile");
		ListSign.add("#");
		ListName.add("Shopping List");
		ListSub.add("View and Edit your shopping list");
		ListSign.add("*");
		ListName.add("Recomended");
		ListSub.add("Products that you should buy");
		ListSign.add("~");
		ListName.add("Lists");
		ListSub.add("Shopping Lists Created By You");
		ListSign.add("-");
	}

	private static final int ZBAR_SCANNER_REQUEST = 0;

	// private static final int ZBAR_QR_SCANNER_REQUEST = 1;

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
		// TODO Auto-generated method stub
		switch (pos) {
		case 0:
			Intent pr = new Intent(getApplicationContext(), Profile.class);
			startActivity(pr);
			break;
		case 1:
			Intent sl = new Intent(getApplicationContext(), ShoppingList.class);
			startActivity(sl);
			break;
		case 2:
			Intent re = new Intent(getApplicationContext(), Recommend.class);
			startActivity(re);
			break;
		case 3:
			Intent li = new Intent(getApplicationContext(), Lists.class);
			startActivity(li);
			break;
		case 4:
			createBarcode("Tej Pratap Singh");
			break;
		case 5:
			Intent zb = new Intent(getApplicationContext(),
					ZBarScannerActivity.class);
			zb.putExtra(ZBarConstants.SCAN_MODES, new int[] { Symbol.QRCODE });
			startActivityForResult(zb, ZBAR_SCANNER_REQUEST);
			break;
		}
	}

	@SuppressLint("NewApi")
	private void createBarcode(String qrInputText) {
		try {
			// Find screen size
			WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
			Display display = manager.getDefaultDisplay();
			Point point = new Point();
			display.getSize(point);
			int width = point.x;
			int height = point.y;
			int smallerDimension = width < height ? width : height;
			smallerDimension = smallerDimension * 3 / 4;
			// Encode with a QR Code image
			QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(qrInputText, null,
					Contents.Type.TEXT, BarcodeFormat.QR_CODE.toString(),
					smallerDimension);
			Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();
			AlertDialog.Builder ad = new AlertDialog.Builder(this);
			ImageView myImage = new ImageView(getApplicationContext());
			myImage.setImageBitmap(bitmap);
			ad.setView(myImage);
			ad.show();
		} catch (WriterException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			Toast.makeText(
					this,
					"Scan Result = "
							+ data.getStringExtra(ZBarConstants.SCAN_RESULT),
					Toast.LENGTH_SHORT).show();
			Toast.makeText(
					this,
					"Scan Result Type = "
							+ data.getIntExtra(ZBarConstants.SCAN_RESULT_TYPE,
									0), Toast.LENGTH_SHORT).show();
		} else if (resultCode == RESULT_CANCELED) {
			// Toast.makeText(this, "Camera unavailable", Toast.LENGTH_SHORT)
			// .show();
		}
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.ibList:
			Intent sl = new Intent(getApplicationContext(), ShoppingList.class);
			startActivity(sl);
			break;
		case R.id.bHomeLogin:
			Intent lo = new Intent(getApplicationContext(), Login.class);
			lo.putExtra("task", "login");
			startActivity(lo);
			break;
		case R.id.bHomeRegister:
			Intent re = new Intent(getApplicationContext(), Login.class);
			re.putExtra("task", "register");
			startActivity(re);
			break;
		case R.id.bHomeViewProfile:
			Intent pr = new Intent(getApplicationContext(), Profile.class);
			startActivity(pr);
			break;
		}
	}
}
