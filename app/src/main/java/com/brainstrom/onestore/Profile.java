package com.brainstrom.onestore;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class Profile extends Activity implements OnClickListener {

	TextView tvUsername, tvUserData;
	Button bProfileLogout, bProfileUpdate;
	EditText etName, etPassword, etBankid, etEmail;
	String _baseurl;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile);

		tvUsername = (TextView) findViewById(R.id.tvProfileUsername);
		tvUserData = (TextView) findViewById(R.id.tvProfileUserData);
		bProfileUpdate = (Button) findViewById(R.id.bProfileUpdate);
		bProfileLogout = (Button) findViewById(R.id.bProfileLogout);
		etName = (EditText) findViewById(R.id.etProfileName);
		etPassword = (EditText) findViewById(R.id.etProfilePassword);
		etBankid = (EditText) findViewById(R.id.etProfileBankId);
		etEmail = (EditText) findViewById(R.id.etProfileEmail);

		ActionBar ab = getActionBar();
		ab.setTitle("One Store");
		ab.setBackgroundDrawable(new ColorDrawable(getResources().getColor(
				R.color.color_actionbar)));

		SharedPreferences pref = getApplicationContext().getSharedPreferences(
				"oneStorePref", 0);
		tvUsername.setText(pref.getString("username", ""));
		String userdata = "Name : " + pref.getString("name", "")
				+ "\nUsername : " + pref.getString("username", "")
				+ "\nEmail : " + pref.getString("email", "") + "\nBank Id : "
				+ pref.getString("bankid", "") + "\nLast Updated List : "
				+ pref.getString("listid", "No List Found");
		tvUserData.setText(userdata);

		bProfileLogout.setOnClickListener(this);
		bProfileUpdate.setOnClickListener(this);

		etName.setText(pref.getString("name", ""));
		etEmail.setText(pref.getString("email", ""));
		etBankid.setText(pref.getString("bankid", ""));

		getData("/customer/customer.currentlist.php?apikey=tejpratap&username="
				+ pref.getString("username", ""));
	}

	private void getBaseUrl() {
		SharedPreferences pref = getApplicationContext().getSharedPreferences(
				"oneStorePref", 0);
		_baseurl = pref.getString("base_url",
				getResources().getString(R.string.base_url));
	}

	private void getData(final String Url) {
		getBaseUrl();
		String url = _baseurl + Url;
		// Instantiate the RequestQueue.
		RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

		// Request a string response from the provided URL.
		StringRequest stringRequest = new StringRequest(Request.Method.GET,
				url, new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						// TODO Auto-generated method stub
						if (Url.contains("customer.currentlist.php")) {
							SetCuttentList(response);
						} else if (Url.contains("customer.update.php")) {
							UpdateData(response);
						}
					}

				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						toast("Error in connection");
					}
				});
		// Add the request to the RequestQueue.
		queue.add(stringRequest);
	}

	private void SetCuttentList(String response) {
		// TODO Auto-generated method stub
		try {
			JSONObject main = new JSONObject(response);
			int status = main.getInt("status");
			if (status == 1) {
				String listid = main.getString("listid");
				String balance = main.getString("balance");
				String last_updated = main.getString("last_updated");
				SharedPreferences pref = getApplicationContext()
						.getSharedPreferences("oneStorePref", 0);
				Editor edit = pref.edit();
				edit.putString("listid", listid);
				edit.commit();
				String userdata = "Name : " + pref.getString("name", "")
						+ "\nUsername : " + pref.getString("username", "")
						+ "\nEmail : " + pref.getString("email", "")
						+ "\nBank Id : " + pref.getString("bankid", "")
						+ "\nLast Updated List : " + listid
						+ "\nCurrent Balance : " + balance
						+ "\nLast Transaction On : " + last_updated;
				tvUserData.setText(userdata);
			} else {
				toast("Error " + main.getString("error"));
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void UpdateData(String response) {
		// TODO Auto-generated method stub
		try {
			JSONObject main = new JSONObject(response);
			int status = main.getInt("status");
			if (status == 1) {
				String name = main.getString("name");
				String username = main.getString("username");
				String email = main.getString("email");
				String bankid = main.getString("bankid");

				toast("Updated");

				SharedPreferences pref = getApplicationContext()
						.getSharedPreferences("oneStorePref", 0);
				Editor edit = pref.edit();
				edit.putString("name", name);
				edit.putString("username", username);
				edit.putString("email", email);
				edit.putString("bankid", bankid);
				edit.commit();

				String userdata = "Name : " + pref.getString("name", "")
						+ "\nUsername : " + pref.getString("username", "")
						+ "\nEmail : " + pref.getString("email", "")
						+ "\nBank Id : " + pref.getString("bankid", "")
						+ "\nLast Updated List : "
						+ pref.getString("listid", "No List Found");
				tvUserData.setText(userdata);
			} else {
				toast("Error " + main.getString("error"));
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		SharedPreferences pref = getApplicationContext().getSharedPreferences(
				"oneStorePref", 0);
		switch (arg0.getId()) {
		case R.id.bProfileLogout:
			Editor edit = pref.edit();
			edit.putString("username", "");
			edit.putString("name", "");
			edit.putString("email", "");
			edit.putString("bankid", "");
			edit.commit();
			finish();
			break;
		case R.id.bProfileUpdate:
			String data = "&username=" + pref.getString("username", "")
					+ "&name=" + etName.getText().toString().replace(" ", "+")
					+ "&password=" + etPassword.getText() + "&email="
					+ etEmail.getText();
			getData("/customer/customer.update.php?apikey=tejpratap" + data);
			break;
		default:
			break;
		}
	}

	private void toast(String str) {
		Toast.makeText(getApplicationContext(), str, Toast.LENGTH_LONG).show();
	}
}