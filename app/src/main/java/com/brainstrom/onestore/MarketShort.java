package com.brainstrom.onestore;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.brainstrom.onestore.adapter.GridViewItemAdapter;

public class MarketShort extends Activity {
	Spinner spBrand, spCategory;
	SeekBar sbMinCost, sbMaxCost;
	EditText etMinCost, etMaxCost;
	GridView gvItems;
	RadioGroup rg;
	RadioButton rb;
	String _baseurl, itemcategory = "", itembrand = "";
	LinearLayout llLoad;
	ArrayList<String> aListCategories = new ArrayList<String>();
	ArrayList<String> aListCategoryBrands = new ArrayList<String>();
	ArrayList<String> aListName = new ArrayList<String>();
	ArrayList<String> aListId = new ArrayList<String>();
	ArrayList<String> aListPrice = new ArrayList<String>();
	ArrayList<String> aListImage = new ArrayList<String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_market_short);
		gvItems = (GridView) findViewById(R.id.gvMarketShortItems);
		llLoad = (LinearLayout) findViewById(R.id.llLoading);

		ActionBar ab = getActionBar();
		ab.setTitle("One Store");
		ab.setBackgroundDrawable(new ColorDrawable(getResources().getColor(
				R.color.color_actionbar)));

		callDatabase("", "", "0", "50000", "totalsold");

		gvItems.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent vi = new Intent(getApplicationContext(), ViewItem.class);
				vi.putExtra("id", aListId.get(arg2));
				startActivity(vi);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.market_short, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.menuSettings:
			showDialog();
			break;
		case R.id.menuViewCart:
			Intent sl = new Intent(getApplicationContext(), ShoppingList.class);
			startActivity(sl);
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void showDialog() {
		getData("/market/market.categories.php?apikey=tejpratap");
		LayoutInflater myLayout = LayoutInflater.from(this);
		final View dialogView = myLayout.inflate(
				R.layout.layout_market_short_settings, null);
		AlertDialog.Builder ad = new AlertDialog.Builder(this);
		ad.setView(dialogView);

		// TODO Seekbars
		spBrand = (Spinner) dialogView.findViewById(R.id.spMarketShortBrand);
		spCategory = (Spinner) dialogView
				.findViewById(R.id.spMarketShortCategory);

		spCategory.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				getData("/market/market.categorybrands.php?apikey=tejpratap&category="
						+ aListCategories.get(arg2));
				itemcategory = aListCategories.get(arg2);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
		spBrand.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				itembrand = aListCategoryBrands.get(arg2);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		// TODO spinners
		sbMinCost = (SeekBar) dialogView
				.findViewById(R.id.sbMarketShortMinPrice);
		sbMaxCost = (SeekBar) dialogView
				.findViewById(R.id.sbMarketShortMaxPrice);
		etMinCost = (EditText) dialogView
				.findViewById(R.id.etMarketShortMinPrice);
		etMaxCost = (EditText) dialogView
				.findViewById(R.id.etMarketShortMaxPrice);

		sbMinCost.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
				// TODO Auto-generated method stub
				etMinCost.setText(Integer.toString(arg1));
			}
		});

		sbMaxCost.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
				// TODO Auto-generated method stub
				etMaxCost.setText(Integer.toString(arg1));
			}
		});
		etMinCost.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				try {
					sbMinCost.setProgress(Integer.parseInt(arg0.toString()));
				} catch (Exception e) {

				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}
		});
		etMaxCost.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				try {
					sbMaxCost.setProgress(Integer.parseInt(arg0.toString()));
				} catch (Exception e) {

				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}
		});

		// TODO radiogroup
		rg = (RadioGroup) dialogView.findViewById(R.id.rgMarketShort);

		ad.setPositiveButton("Short", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				// TODO Auto-generated method stub
				try {
					int selected = rg.getCheckedRadioButtonId();
					rb = (RadioButton) dialogView.findViewById(selected);
					if (rb.getText().toString().contains("Top Selling")) {
						callDatabase(itemcategory, itembrand,
								Integer.toString(sbMinCost.getProgress()),
								Integer.toString(sbMaxCost.getProgress()),
								"totalsold");
					} else {
						callDatabase(itemcategory, itembrand,
								Integer.toString(sbMinCost.getProgress()),
								Integer.toString(sbMaxCost.getProgress()),
								"date");
					}
				} catch (Exception e) {
					toast("Error " + e.getMessage());
				}
			}
		});
		ad.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				// TODO Auto-generated method stub
				arg0.dismiss();
			}
		});
		ad.show();
	}

	private void clear() {
		aListId.clear();
		aListName.clear();
		aListImage.clear();
		aListPrice.clear();
	}

	private void callDatabase(String itemcategory, String itembrand,
			String minprice, String maxprice, String orderby) {
		String url = "";
		if (itemcategory.length() < 1) {
			url = "/query.php?apikey=tejpratap&query=SELECT itemid,itemname,itemprice,imageurl FROM `market` WHERE `itemprice` BETWEEN "
					+ minprice
					+ " AND "
					+ maxprice
					+ " ORDER BY `"
					+ orderby
					+ "` DESC";
			Log.d("url1", url);
		} else if (itembrand.length() > 1) {
			url = "/query.php?apikey=tejpratap&query=SELECT itemid,itemname,itemprice,imageurl FROM `market` WHERE `itemcategory`='"
					+ itemcategory
					+ "' AND `itembrand`='"
					+ itembrand
					+ "' AND `itemprice` BETWEEN "
					+ minprice
					+ " AND "
					+ maxprice + " ORDER BY `" + orderby + "` DESC";
			Log.d("url2", url);
		} else {
			url = "/query.php?apikey=tejpratap&query=SELECT itemid,itemname,itemprice,imageurl FROM `market` WHERE `itemcategory`='"
					+ itemcategory
					+ "' AND `itemprice` BETWEEN "
					+ minprice
					+ " AND " + maxprice + " ORDER BY `" + orderby + "` DESC";
			Log.d("url3", url);
		}
		llLoad.setVisibility(View.VISIBLE);
		getData(url.replace(" ", "+"));
	}

	private void getBaseUrl() {
		SharedPreferences pref = getApplicationContext().getSharedPreferences(
				"oneStorePref", 0);
		_baseurl = pref.getString("base_url",
				getResources().getString(R.string.base_url));
	}

	private void getData(final String Url) {
		getBaseUrl();
		String url = _baseurl + Url;
		Log.d("mainurl", url);
		// Instantiate the RequestQueue.
		RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

		// Request a string response from the provided URL.
		StringRequest stringRequest = new StringRequest(Request.Method.GET,
				url, new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						// TODO Auto-generated method stub
						Log.d("data1", response);
						if (Url.contains("market.categories.php")) {
							addCategoriesOnSpinner(response);
						} else if (Url.contains("market.categorybrands.php")) {
							addBrandsOnSpinner(response);
						} else {
							setData(response);
							Log.d("data2", response);
							llLoad.setVisibility(View.GONE);
						}
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						toast("No internet connection");
					}
				});
		// Add the request to the RequestQueue.
		queue.add(stringRequest);
	}

	private void addCategoriesOnSpinner(String response) {
		try {
			aListCategories.clear();
			JSONObject main = new JSONObject(response);
			int status = main.getInt("status");
			if (status == 1) {
				JSONArray jar = main.getJSONArray("categories");
				for (int i = 0; i < jar.length(); i++) {
					aListCategories.add(jar.getString(i));
				}
				ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
						this, android.R.layout.simple_spinner_item,
						aListCategories);
				dataAdapter
						.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				spCategory.setAdapter(dataAdapter);
			} else {
				toast("Error " + main.getString("error"));
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void addBrandsOnSpinner(String response) {
		try {
			aListCategoryBrands.clear();
			JSONObject main = new JSONObject(response);
			int status = main.getInt("status");
			if (status == 1) {
				JSONArray jar = main.getJSONArray("brands");
				for (int i = 0; i < jar.length(); i++) {
					aListCategoryBrands.add(jar.getString(i));
				}
				ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
						this, android.R.layout.simple_spinner_item,
						aListCategoryBrands);
				dataAdapter
						.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				spBrand.setAdapter(dataAdapter);
			} else {
				toast("Error " + main.getString("error"));
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void setData(String response) {
		try {
			clear();
			JSONArray main = new JSONArray(response);
			for (int i = 0; i < main.length(); i++) {
				JSONArray item = main.getJSONArray(i);
				aListId.add(item.getString(0));
				aListName.add(item.getString(1));
				aListPrice.add(item.getString(2));
				aListImage.add(item.getString(3));
			}
			// gvItems.setAdapter(new GridSingleItemAdd(getApplicationContext(),
			// aListImage, aListId, aListName, aListPrice));
			gvItems.setAdapter(new GridViewItemAdapter(this, aListImage,
					aListId, aListName, aListPrice));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void toast(String str) {
		Toast.makeText(getApplicationContext(), str, Toast.LENGTH_LONG).show();
	}
}
