package com.brainstrom.onestore;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.brainstrom.onestore.database.ShoppingListDatabaseHandler;
import com.brainstrom.onestore.database.ShoppingListFrame;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.loopj.android.image.SmartImageView;

public class Recommend extends Activity implements OnClickListener {

	String _baseurl;
	TextView tvNameCost, tvWantToAdd;
	Button bYes, bNo;
	ArrayList<String> aListId = new ArrayList<String>();
	String itemid, itemname, itemprice, itemdiscreption, itemspecification,
			itemcategory, itembrand, quantity, imageurl, itemlocation, date,
			qrcode;
	SmartImageView iv;
	int pos;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_recommend);
        getActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.color_actionbar)));
		tvNameCost = (TextView) findViewById(R.id.tvRecommendNamePrice);
		tvWantToAdd = (TextView) findViewById(R.id.tvRecommendWantAdd);
		bYes = (Button) findViewById(R.id.bRecommendAdd);
		bNo = (Button) findViewById(R.id.bRecommendDontAdd);
		iv = (SmartImageView) findViewById(R.id.ivRecommend);
		SharedPreferences pref = getApplicationContext().getSharedPreferences(
				"oneStorePref", 0);
		getData("/customer/customer.recommend.php?apikey=tejpratap&username="
				+ pref.getString("username", ""));

		bYes.setOnClickListener(this);
		bNo.setOnClickListener(this);
		iv.setOnTouchListener(new com.brainstrom.onestore.views.OnSwipeTouchListener(
				this) {
			public void onSwipeTop() {
				// toast("top");
			}

			public void onSwipeRight() {
				// toast("right");
				loadNext();
			}

			public void onSwipeLeft() {
				// toast("left");
				loadPre();
			}

			public void onSwipeBottom() {
				// toast("bottom");
			}

		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.view_item, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.menuViewCart:
			Intent sl = new Intent(getApplicationContext(), ShoppingList.class);
			startActivity(sl);
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void addToShopping() {
		try {
			int id = Integer.parseInt(itemid);
			ShoppingListDatabaseHandler db = new ShoppingListDatabaseHandler(
					getApplicationContext());
			boolean has = db.Exists(id);
			if (has) {
				ShoppingListFrame sl = db.getshoppingList(id);
				db.updateshoppingList(new ShoppingListFrame(id, itemname,
						itemprice, sl.getQuantity() + 1));

				Log.d("added2",
						"Id : " + id + " Name : " + itemname + " Cost : "
								+ itemprice + " Quantity : " + sl.getQuantity()
								+ 1);
			} else {
				db.addshoppingList(new ShoppingListFrame(id, itemname,
						itemprice, 1));
				Log.d("added3", "Id : " + id + " Name : " + itemname
						+ " Cost : " + itemprice + " Quantity : " + 1);
			}
			toast(itemname + " Added to shopping list");
		} catch (Exception e) {
			toast("Error  : " + e.getMessage());
		}
	}

	private void getBaseUrl() {
		SharedPreferences pref = getApplicationContext().getSharedPreferences(
				"oneStorePref", 0);
		_baseurl = pref.getString("base_url",
				getResources().getString(R.string.base_url));
	}

	private void getData(final String Url) {
		getBaseUrl();
		String url = _baseurl + Url;
		// Instantiate the RequestQueue.
		RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

		// Request a string response from the provided URL.
		StringRequest stringRequest = new StringRequest(Request.Method.GET,
				url, new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						// TODO Auto-generated method stub
						if (Url.contains("customer.recommend.php")) {
							setRecommendData(response);
						} else {
							setMarketData(response);
						}
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						toast("No internet connection");
					}
				});
		// Add the request to the RequestQueue.
		queue.add(stringRequest);
	}

	private void setRecommendData(String response) {
		try {
			JSONObject main = new JSONObject(response);
			int status = main.getInt("status");
			if (status == 1) {
				JSONArray jar = main.getJSONArray("items");
				for (int i = 0; i < jar.length(); i++) {
					aListId.add(jar.getString(i));
				}
			} else {
				toast("Error " + main.getString("error"));
			}
			if (aListId.size() > 0) {
				pos = 0;
				getData("/market/market.show.php?apikey=tejpratap&itemid="
						+ aListId.get(pos));
			} else {
				toast("Your dont have any previous purchase records");
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void setMarketData(String response) {
		try {
			JSONObject main = new JSONObject(response);
			int status = main.getInt("status");
			if (status == 1) {
				itemid = main.getString("itemid");
				itemname = main.getString("itemname");
				itemprice = "Rs. " + main.getString("itemprice");
				itemdiscreption = main.getString("itemdiscreption");
				itemspecification = main.getString("itemspecification");
				itemcategory = main.getString("itemcategory");
				itembrand = main.getString("itembrand");
				quantity = main.getString("quantity");
				imageurl = main.getString("imageurl");
				itemlocation = main.getString("itemlocation");
				date = main.getString("date");
				qrcode = main.getString("qrcode");
				setData();
			} else {
				toast("Error " + main.getString("error"));
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void setData() {
		tvNameCost.setText(itemname + "  (" + itemprice + ")");
		tvWantToAdd.setText("Do you want to add " + itemname
				+ " to your shopping list ?");
		iv.setImageUrl(imageurl);
	}

	private void clear() {
		iv.setImageResource(R.drawable.cart_dark);
		tvNameCost.setText("Loading...");
		tvWantToAdd.setText("Loading...");
	}

	private void loadNext() {
		if (pos < aListId.size() - 1) {
			pos++;
			YoYo.with(Techniques.SlideInLeft).duration(500).delay(100)
					.playOn(iv);
			clear();
			getData("/market/market.show.php?apikey=tejpratap&itemid="
					+ aListId.get(pos));
		}
	}

	private void loadPre() {
		if (pos > 0) {
			pos--;
			YoYo.with(Techniques.SlideInRight).duration(500).delay(100)
					.playOn(iv);
			clear();
			getData("/market/market.show.php?apikey=tejpratap&itemid="
					+ aListId.get(pos));
		}
	}

	private void toast(String str) {
		Toast.makeText(getApplicationContext(), str, Toast.LENGTH_LONG).show();
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.bRecommendAdd:
			addToShopping();
			loadNext();
			break;
		case R.id.bRecommendDontAdd:
			loadNext();
			break;
		}
	}

}
