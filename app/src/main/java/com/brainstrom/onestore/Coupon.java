package com.brainstrom.onestore;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;


public class Coupon extends Activity implements View.OnClickListener {

    EditText etCode;
    WebView wvList;
    Button bSkip, bApply;
    LinearLayout llLoad;
    public static String _baseurl, listid, items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon);
        bSkip = (Button) findViewById(R.id.bCouponSkip);
        bApply = (Button) findViewById(R.id.bCouponApply);
        wvList = (WebView) findViewById(R.id.wvCouponList);
        etCode = (EditText) findViewById(R.id.etCouponCode);
        llLoad = (LinearLayout) findViewById(R.id.llLoading);

        getActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(
                R.color.color_actionbar)));

        if (getIntent().hasExtra("listid")) {
            listid = getIntent().getStringExtra("listid");
            if (getIntent().hasExtra("items")) {
                items = getIntent().getStringExtra("items");
                wvList.loadData(items, "text/html", "UTF-8");
            } else {
                getData("lists/lists.show.php?apikey=tejpratap&listid=" + listid);
            }
        }

        bApply.setOnClickListener(this);
        bSkip.setOnClickListener(this);
    }

    private void getBaseUrl() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "oneStorePref", 0);
        _baseurl = pref.getString("base_url",
                getResources().getString(R.string.base_url));
    }

    private void getData(final String Url) {
        getBaseUrl();
        String url = _baseurl + Url;
        llLoad.setVisibility(View.VISIBLE);
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // TODO Auto-generated method stub
                Log.d("Coupon Url", Url);
                Log.d("Coupon Response", response);
                if (Url.contains("lists.show.php")) {
                    try {
                        if (new JSONObject(response).getInt("status") == 1) {
                            String items = new JSONObject(response).getString("items");
                            wvList.loadData(tabularizeList(items), "text/html", "UTF-8");
                        } else {
                            toast(new JSONObject(response).getString("error"));
                        }
                    } catch (JSONException e) {
                    }
                } else if (Url.contains("coupon.check.php")) {
                    try {
                        JSONObject main = new JSONObject(response);
                        if (main.getInt("status") == 1) {
                            String items = tabularizeList(main.getString("items"));
                            if (items.length() > 5) {
                                Coupon.items = items;
                            }
                            wvList.loadData(items, "text/html", "UTF-8");
                            toast("Coupon Applied, check above list is updated");
                            bSkip.setText("Next Step");
                        } else {
                            toast(main.getString("error"));
                        }
                    } catch (Exception e) {
                    }
                }
                llLoad.setVisibility(View.GONE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                toast("No internet connection");
                llLoad.setVisibility(View.GONE);
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private String tabularizeList(String items) {
        Document doc = Jsoup.parse(items);
        Elements ids = doc.getElementsByTag("id");
        Elements names = doc.getElementsByTag("name");
        Elements qtys = doc.getElementsByTag("quantity");
        Elements costs = doc.getElementsByTag("cost");

        int total = 0;
        String itemlist = "<style>table,th,td{border:1px solid black;border-collapse: collapse;}th,td{padding: 5px;}th{text-align: left;}</style>";
        itemlist = "<table style='width:100%;' >";
        itemlist = itemlist
                + "<tr><td>name</td><td>price</td><td>qty</td></tr>";
        for (int j = 0; j < ids.size(); j++) {
            itemlist = itemlist + "<tr>";
            itemlist = itemlist + "<td>" + names.get(j).text() + "</td>"
                    + "<td>" + costs.get(j).text() + "</td>" + "<td>"
                    + qtys.get(j).text() + "</td>";
            itemlist = itemlist + "</tr>";
            total = total
                    + (Integer.parseInt(costs.get(j).text()) * Integer
                    .parseInt(qtys.get(j).text()));
        }
        itemlist = itemlist + "<td>total : </td>" + "<td>"
                + Integer.toString(total) + "</td>" + "<td></td>";
        itemlist = itemlist + "</table>";
        return itemlist;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bCouponApply:
                String code = etCode.getText().toString();
                if (code.length() > 2) {
                    getData("coupon/coupon.check.php?apikey=tejpratap&listid=" + listid + "&coupon=" + etCode.getText().toString().replace(" ", "+"));
                } else {
                    Toast.makeText(getApplicationContext(), "code should be greater then 2 letters", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.bCouponSkip:
                Intent i = new Intent(getApplicationContext(), Payment.class);
                i.putExtra("items", items);
                i.putExtra("listid", listid);
                startActivity(i);
                break;
        }
    }

    private void toast(String str) {
        Toast.makeText(getApplicationContext(), str, Toast.LENGTH_LONG).show();
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_coupon, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
}
