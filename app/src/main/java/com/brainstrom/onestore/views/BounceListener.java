package com.brainstrom.onestore.views;

import com.brainstrom.onestore.views.BounceScroller.State;

public interface BounceListener {

	public void onState(boolean header, State state);

	public void onOffset(boolean header, int offset);

}