package com.brainstrom.onestore;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.brainstrom.onestore.adapter.GridViewItemAdapter;
import com.brainstrom.onestore.database.ShoppingListDatabaseHandler;
import com.brainstrom.onestore.database.ShoppingListFrame;

import net.sourceforge.zbar.Symbol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.Locale;

public class Search extends Activity implements OnClickListener {

    String _baseurl;
    ListView lvSuggestion;
    AutoCompleteTextView etSearch;
    ImageButton ibClear;
    GridView gvSuggestion;
    Button bBarcode, bQrcode;
    ArrayList<String> aListName = new ArrayList<String>();
    ArrayList<String> aListId = new ArrayList<String>();
    ArrayList<String> aListPrice = new ArrayList<String>();
    ArrayList<String> aListImage = new ArrayList<String>();
    LinearLayout llLoad;
    ImageButton ibVoiceSearch;
    private final int REQ_CODE_SPEECH_INPUT = 100;
    private static final int ZBAR_SCANNER_REQUEST = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        lvSuggestion = (ListView) findViewById(R.id.lvSearchSuggestion);
        gvSuggestion = (GridView) findViewById(R.id.gvSearchSuggestion);
        etSearch = (AutoCompleteTextView) findViewById(R.id.etSearchSearch);
        ibClear = (ImageButton) findViewById(R.id.ibSearchClear);
        llLoad = (LinearLayout) findViewById(R.id.llLoading);
        ibVoiceSearch = (ImageButton) findViewById(R.id.ibSearchVoiceSearch);
        bBarcode = (Button) findViewById(R.id.bSearchBarcode);
        bQrcode = (Button) findViewById(R.id.bSearchQrcode);

        ActionBar ab = getActionBar();
        ab.setTitle("Search");
        ab.setBackgroundDrawable(new ColorDrawable(getResources().getColor(
                R.color.color_actionbar)));

        etSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                if (arg0.length() > 2) {
                    lvSuggestion.setVisibility(View.VISIBLE);
                    // getData("/market/market.searchquick.php?apikey=tejpratap&q="
                    // + arg0.toString().replace(" ", "+"), 1);
                    getData("/market/market.search1.php?apikey=tejpratap&q="
                            + arg0.toString().replace(" ", "+"), 1);
                } else {
                    lvSuggestion.setVisibility(View.GONE);
                }
            }
        });

        ibClear.setOnClickListener(this);
        ibVoiceSearch.setOnClickListener(this);
        bBarcode.setOnClickListener(this);
        bQrcode.setOnClickListener(this);

        lvSuggestion.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                // TODO Auto-generated method stub
                Intent vi = new Intent(getApplicationContext(), ViewItem.class);
                vi.putExtra("id", aListId.get(arg2));
                startActivity(vi);
            }
        });
        gvSuggestion.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                // TODO Auto-generated method stub
                Intent vi = new Intent(getApplicationContext(), ViewItem.class);
                vi.putExtra("id", aListId.get(arg2));
                startActivity(vi);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // TODO Auto-generated method stub
        getMenuInflater().inflate(R.menu.search, menu);
        SearchView search = (SearchView) menu.findItem(R.id.menuSearch)
                .getActionView();
        search.setOnQueryTextListener(new OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String arg0) {
                // TODO Auto-generated method stub
                return false;
            }

            @Override
            public boolean onQueryTextChange(String arg0) {
                // TODO Auto-generated method stub
                if (arg0.length() > 3) {
                    lvSuggestion.setVisibility(View.VISIBLE);
                    getData("/market/market.searchquick.php?apikey=tejpratap&q="
                            + arg0.replace(" ", "+"), 1);
                } else {
                    lvSuggestion.setVisibility(View.GONE);
                }
                return true;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    private void clear() {
        aListId.clear();
        aListName.clear();
        aListImage.clear();
        aListPrice.clear();
    }

    private void getBaseUrl() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "oneStorePref", 0);
        _baseurl = pref.getString("base_url",
                getResources().getString(R.string.base_url));
    }

    private void getData(final String Url, int page) {
        getBaseUrl();
        llLoad.setVisibility(View.VISIBLE);
        String url = _baseurl + Url + "&page=" + page;
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // TODO Auto-generated method stub
                if (Url.contains("searchquick")) {
                    searchSeggestionList(response);
                } else {
                    searchSeggestionGrid(response);
                }
                llLoad.setVisibility(View.GONE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                toast("No internet connection");
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void searchSeggestionList(String response) {
        try {
            clear();
            JSONObject main = new JSONObject(response);
            JSONArray jar = main.getJSONArray("search");
            if (jar.length() == 0) {
                toast("No item found");
            } else {
                for (int i = 0; i < jar.length(); i++) {
                    JSONObject search = jar.getJSONObject(i);
                    String Name = search.getString("itemname");
                    String Id = search.getString("itemid");
                    aListId.add(Id);
                    aListName.add(Name);
                }
                lvSuggestion.setAdapter(new ArrayAdapter<String>(
                        getApplicationContext(),
                        android.R.layout.simple_list_item_1, aListName));
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void searchSeggestionGrid(String response) {
        try {
            clear();
            JSONObject main = new JSONObject(response);
            JSONArray jar = main.getJSONArray("search");
            if (jar.length() == 0) {
                toast("No item found");
            } else {
                for (int i = 0; i < jar.length(); i++) {
                    JSONObject search = jar.getJSONObject(i);
                    aListId.add(search.getString("itemid"));
                    aListName.add(search.getString("itemname"));
                    aListPrice.add(search.getString("itemprice"));
                    aListImage.add(search.getString("imageurl"));
                }
                // gvSuggestion.setAdapter(new GridSingleItemAdd(
                // getApplicationContext(), aListImage, aListId,
                // aListName, aListPrice));
                gvSuggestion.setAdapter(new GridViewItemAdapter(this,
                        aListImage, aListId, aListName, aListPrice));
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void toast(String str) {
        Toast.makeText(getApplicationContext(), str, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View arg0) {
        // TODO Auto-generated method stub
        switch (arg0.getId()) {
            case R.id.ibSearchClear:
                etSearch.setText("");
                break;
            case R.id.ibSearchVoiceSearch:
                promptSpeechInput();
                break;
            case R.id.bSearchBarcode:
                Intent zb = new Intent(getApplicationContext(),
                        ZBarScannerActivity.class);
                zb.putExtra(ZBarConstants.SCAN_MODES, new int[]{Symbol.UPCA, Symbol.UPCE});
                startActivityForResult(zb, ZBAR_SCANNER_REQUEST);
                break;
            case R.id.bSearchQrcode:
                Intent zq = new Intent(getApplicationContext(),
                        ZBarScannerActivity.class);
                zq.putExtra(ZBarConstants.SCAN_MODES, new int[]{Symbol.QRCODE});
                startActivityForResult(zq, ZBAR_SCANNER_REQUEST);
                break;
        }
    }

    // requesting voice input

    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Say something");
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    "Sorry! Your device doesn\'t support speech input",
                    Toast.LENGTH_SHORT).show();
        }
    }

    // Receiving speech input

    @SuppressLint("DefaultLocale")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    toast("Searchin For : " + result.get(0));
                    getData("/market/market.search1.php?apikey=tejpratap&q="
                            + result.get(0).toString().replace(" ", "+"), 1);
                }
                break;
            }
            case ZBAR_SCANNER_REQUEST: {
                if (resultCode == RESULT_OK) {
                    String scannerData = data
                            .getStringExtra(ZBarConstants.SCAN_RESULT);
                    showDialogForBarcode(scannerData);
                    Log.d("Scanner Data", scannerData);
                } else if (resultCode == RESULT_CANCELED) {
                    // Toast.makeText(this, "Camera unavailable",
                    // Toast.LENGTH_SHORT)
                    // .show();
                }
            }
        }
    }

    private void showDialogForBarcode(String data) {
        AlertDialog.Builder alert = new AlertDialog.Builder(Search.this);
        LayoutInflater factory = LayoutInflater.from(Search.this);
        View layout = factory.inflate(R.layout.layout_scan_result, null);
        TextView tvName = (TextView) layout
                .findViewById(R.id.tvLayoutScanResultName);
        TextView tvCost = (TextView) layout
                .findViewById(R.id.tvLayoutScanResultCost);
        if (data.contains("<id>") && data.contains("<name>")
                && data.contains("<cost>")) {
            Document doc = Jsoup.parse(data);
            final String id = doc.getElementsByTag("id").first().text();
            final String name = doc.getElementsByTag("name").first().text();
            final String cost = doc.getElementsByTag("cost").first().text();
            tvCost.setText(cost);
            tvName.setText(name);
            alert.setPositiveButton("View Item",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            // TODO Auto-generated method stub
                            Intent i = new Intent(getApplicationContext(),
                                    ViewItem.class);
                            i.putExtra("id", id);
                            startActivity(i);
                        }
                    });
            alert.setNeutralButton("Add To List",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            addToShopping(id, name, cost);
                        }
                    });
        }
        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                // TODO Auto-generated method stub
                arg0.dismiss();
            }
        });
        alert.setView(layout);
        alert.show();
    }

    private void addToShopping(String iid, String name, String cost) {
        try {
            int id = Integer.parseInt(iid);
            ShoppingListDatabaseHandler db = new ShoppingListDatabaseHandler(
                    getApplicationContext());
            boolean has = db.Exists(id);
            if (has) {
                ShoppingListFrame sl = db.getshoppingList(id);
                db.updateshoppingList(new ShoppingListFrame(id, name, cost, sl
                        .getQuantity() + 1));

                Log.d("added2", "Id : " + id + " Name : " + name + " Cost : "
                        + cost + " Quantity : " + sl.getQuantity() + 1);
            } else {
                db.addshoppingList(new ShoppingListFrame(id, name, cost, 1));
                Log.d("added3", "Id : " + id + " Name : " + name + " Cost : "
                        + cost + " Quantity : " + 1);
            }
        } catch (Exception e) {
            toast("Error  : " + e.getMessage());
        }
    }
}
