package com.brainstrom.onestore;

import java.util.ArrayList;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.brainstrom.onestore.adapter.MenuListAdapter;

public class MenuSectionFragment extends Fragment implements OnClickListener,
		OnItemClickListener {

	LinearLayout llLogin, llAfterLogin, llManage;
	ImageButton ibShoppingList;
	Button bRegister, bLogin, bProfile;
	ListView lvMenu;
	TextView tvUserDetails;
	ArrayList<String> ListName = new ArrayList<String>();
	ArrayList<String> ListSub = new ArrayList<String>();
	ArrayList<String> ListSign = new ArrayList<String>();

	public MenuSectionFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_home_menu,
				container, false);
		llLogin = (LinearLayout) rootView.findViewById(R.id.llHomeLogin);
		llAfterLogin = (LinearLayout) rootView
				.findViewById(R.id.llHomeAfterLogin);
		llManage = (LinearLayout) rootView.findViewById(R.id.llHomeManage);
		ibShoppingList = (ImageButton) rootView.findViewById(R.id.ibList);
		lvMenu = (ListView) rootView.findViewById(R.id.lvHomeMenu);
		bLogin = (Button) rootView.findViewById(R.id.bHomeLogin);
		bRegister = (Button) rootView.findViewById(R.id.bHomeRegister);
		bProfile = (Button) rootView.findViewById(R.id.bHomeViewProfile);
		tvUserDetails = (TextView) rootView.findViewById(R.id.tvHomeUserData);
		ibShoppingList.setOnClickListener(this);
		clearList();
		setList();
		lvMenu.setAdapter(new MenuListAdapter(getActivity(), ListName, ListSub,
				ListSign));
		lvMenu.setOnItemClickListener(this);
		bLogin.setOnClickListener(this);
		bRegister.setOnClickListener(this);
		bProfile.setOnClickListener(this);

		return rootView;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		SharedPreferences pref = getActivity().getSharedPreferences(
				"oneStorePref", 0);
		if (pref.getString("username", "").length() > 0) {
			llAfterLogin.setVisibility(View.VISIBLE);
			llLogin.setVisibility(View.GONE);
			tvUserDetails.setText("Welcome " + pref.getString("username", "") + "\nName : " + pref.getString("name", ""));
		} else {
			llLogin.setVisibility(View.VISIBLE);
			llAfterLogin.setVisibility(View.GONE);
		}
	}

	private void clearList() {
		ListSub.clear();
		ListName.clear();
		ListSign.clear();
	}

	private void setList() {
		ListName.add("Manage List");
		ListSub.add("Manage Your Shopping Lists");
		ListSign.add("#");
		ListName.add("Scan Item");
		ListSub.add("Scan Product");
		ListSign.add("*");
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.ibList:
			Intent sl = new Intent(getActivity(), ShoppingList.class);
			startActivity(sl);
			break;
		case R.id.bHomeLogin:
			Intent lo = new Intent(getActivity(), Login.class);
			lo.putExtra("task", "login");
			startActivity(lo);
			break;
		case R.id.bHomeRegister:
			Intent re = new Intent(getActivity(), Login.class);
			re.putExtra("task", "register");
			startActivity(re);
			break;
		case R.id.bHomeViewProfile:
			Intent pr = new Intent(getActivity(), Profile.class);
			startActivity(pr);
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
		// TODO Auto-generated method stub
		switch (pos) {
		case 1:
			break;
		}
	}
}