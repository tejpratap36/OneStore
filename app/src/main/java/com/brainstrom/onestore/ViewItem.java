package com.brainstrom.onestore;

import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.brainstrom.onestore.database.ShoppingListDatabaseHandler;
import com.brainstrom.onestore.database.ShoppingListFrame;
import com.daimajia.swipe.SwipeLayout;
import com.loopj.android.image.SmartImageView;

@SuppressWarnings("deprecation")
public class ViewItem extends FragmentActivity implements ActionBar.TabListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
     * will keep every loaded fragment in memory. If this becomes too memory
     * intensive, it may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;
    ActionBar ab;
    static String id, _baseurl, itemid, itemname, itemprice, itemdiscreption, itemspecification, itemcategory, itembrand, quantity, imageurl, itemlocation, date, qrcode;
    static TextView tvName, tvCost, tvName2, tvPrice2, tvCategory2, tvBrand2,
            tvQuantity2, tvLocation2, tvDescription2;
    static Button bAdd;
    static SmartImageView ivImage, ivQrcode;
    static LinearLayout llSpecifications;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_item);

        // Set up the action bar.
        final ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the app.
        mSectionsPagerAdapter = new SectionsPagerAdapter(
                getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        // When swiping between different sections, select the corresponding
        // tab. We can also use ActionBar.Tab#select() to do this if we have
        // a reference to the Tab.
        mViewPager
                .setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
                    @Override
                    public void onPageSelected(int position) {
                        actionBar.setSelectedNavigationItem(position);
                    }
                });

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
            // Create a tab with text corresponding to the page title defined by
            // the adapter. Also specify this Activity object, which implements
            // the TabListener interface, as the callback (listener) for when
            // this tab is selected.
            actionBar.addTab(actionBar.newTab()
                    .setText(mSectionsPagerAdapter.getPageTitle(i))
                    .setTabListener(this)
                    .setIcon(mSectionsPagerAdapter.getPageIcon(i)));
        }

        ab = getActionBar();
        ab.setTitle("One Store");
        ab.setBackgroundDrawable(new ColorDrawable(getResources().getColor(
                R.color.color_actionbar)));
        if (getIntent().hasExtra("id")) {
            id = getIntent().getStringExtra("id");
            getData();
        }

        bAdd = (Button) findViewById(R.id.bViewItemAdd);
        bAdd.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                addToShopping();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.view_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {
            case R.id.menuViewCart:
                Intent sl = new Intent(getApplicationContext(), ShoppingList.class);
                startActivity(sl);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab,
                              FragmentTransaction fragmentTransaction) {
        // When the given tab is selected, switch to the corresponding page in
        // the ViewPager.
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab,
                                FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab,
                                FragmentTransaction fragmentTransaction) {
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a DummySectionFragment (defined as a static inner class
            // below) with the page number as its lone argument.
            Fragment fragment = null;
            switch (position) {
                case 0:
                    fragment = new Page1Fragment();
                    break;
                case 1:
                    fragment = new Page2Fragment();
                    break;
                case 2:
                    fragment = new Page3Fragment();
                    break;
                default:
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return "View".toUpperCase(l);
                case 1:
                    return "Details".toUpperCase(l);
                case 2:
                    return "Specifications".toUpperCase(l);
            }
            return null;
        }

        private int getPageIcon(int position) {
            switch (position) {
                case 0:
                    return R.drawable.view1;
                case 1:
                    return R.drawable.details1;
                case 2:
                    return R.drawable.specs1;
            }
            return R.drawable.featured;
        }
    }

    // TODO page 1
    public static class Page1Fragment extends Fragment {

        public Page1Fragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_view_item_page1,
                    container, false);
            SwipeLayout swipeLayout = (SwipeLayout) rootView
                    .findViewById(R.id.swipe);
            swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
            swipeLayout.setDragEdge(SwipeLayout.DragEdge.Top);
            tvName = (TextView) rootView.findViewById(R.id.tvViewItemName);
            tvCost = (TextView) rootView.findViewById(R.id.tvViewItemPrice);
            ivImage = (SmartImageView) rootView
                    .findViewById(R.id.ivViewItemImage);
            ivQrcode = (SmartImageView) rootView
                    .findViewById(R.id.ivViewItemQrcode);

            tvName.setText(itemname);
            tvCost.setText(itemprice);
            ivQrcode.setImageUrl(qrcode);
            ivImage.setImageUrl(imageurl);
            return rootView;
        }
    }

    // TODO page 2
    public static class Page2Fragment extends Fragment {

        public Page2Fragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_view_item_page2,
                    container, false);
            tvName2 = (TextView) rootView
                    .findViewById(R.id.tvViewItemPage2Name);
            tvBrand2 = (TextView) rootView
                    .findViewById(R.id.tvViewItemPage2Brand);
            tvPrice2 = (TextView) rootView
                    .findViewById(R.id.tvViewItemPage2Price);
            tvCategory2 = (TextView) rootView
                    .findViewById(R.id.tvViewItemPage2Category);
            tvLocation2 = (TextView) rootView
                    .findViewById(R.id.tvViewItemPage2Location);
            tvQuantity2 = (TextView) rootView
                    .findViewById(R.id.tvViewItemPage2Quantity);
            tvDescription2 = (TextView) rootView
                    .findViewById(R.id.tvViewItemPage2Description);

            tvName2.setText(itemname);
            tvPrice2.setText(itemprice);
            tvCategory2.setText(itemcategory);
            tvLocation2.setText(itemlocation);
            tvBrand2.setText(itembrand);
            tvQuantity2.setText(quantity);
            tvDescription2.setText(itemdiscreption);
            return rootView;
        }
    }

    // TODO page 3
    public static class Page3Fragment extends Fragment {

        public Page3Fragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_view_item_page3,
                    container, false);
            llSpecifications = (LinearLayout) rootView.findViewById(R.id.llViewItemPage3Specification);

            try {
                String[] specs = itemspecification.split(",");
                for (int i = 0; i < specs.length; i++) {
                    TextView tv = new TextView(getActivity());
                    tv.setBackgroundResource(R.drawable.fb_card);
                    tv.setTextSize(20);
                    tv.setPadding(10, 10, 10, 10);
                    tv.setTextColor(getResources().getColor(R.color.bg_gradient_end));
                    tv.setText(specs[i]);
                    llSpecifications.addView(tv);
                }
            } catch (Exception e) {

            }
            return rootView;
        }
    }

    private void addToShopping() {
        try {
            int id = Integer.parseInt(itemid);
            ShoppingListDatabaseHandler db = new ShoppingListDatabaseHandler(
                    getApplicationContext());
            boolean has = db.Exists(id);
            if (has) {
                ShoppingListFrame sl = db.getshoppingList(id);
                db.updateshoppingList(new ShoppingListFrame(id, itemname, itemprice, sl.getQuantity() + 1));

                Log.d("added2", "Id : " + id + " Name : " + itemname
                        + " Cost : " + itemprice + " Quantity : "
                        + sl.getQuantity() + 1);
            } else {
                db.addshoppingList(new ShoppingListFrame(id, itemname, itemprice, 1));
                Log.d("added3", "Id : " + id + " Name : " + itemname
                        + " Cost : " + itemprice + " Quantity : " + 1);
            }
            toast(itemname + " Added to shopping list");
        } catch (Exception e) {
            toast("Error  : " + e.getMessage());
        }
    }

    private void getBaseUrl() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "oneStorePref", 0);
        _baseurl = pref.getString("base_url",
                getResources().getString(R.string.base_url));
    }

    private void getData() {
        getBaseUrl();
        String url = _baseurl
                + "/market/market.show.php?apikey=tejpratap&itemid=" + id;
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // TODO Auto-generated method stub
                setData(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                toast("No internet connection");
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void setData(String response) {
        try {
            JSONObject main = new JSONObject(response);
            itemid = main.getString("itemid");
            itemname = main.getString("itemname");
            itemprice = "Rs. " + main.getString("itemprice");
            itemdiscreption = main.getString("itemdiscreption");
            itemspecification = main.getString("itemspecification");
            itemcategory = main.getString("itemcategory");
            itembrand = main.getString("itembrand");
            quantity = main.getString("quantity");
            imageurl = main.getString("imageurl");
            itemlocation = main.getString("itemlocation");
            date = main.getString("date");
            qrcode = main.getString("qrcode");

            ab.setTitle(itemname);
            ab.setSubtitle(itembrand);

            tvName.setText(itemname);
            tvCost.setText(itemprice);
            ivQrcode.setImageUrl(qrcode);
            ivImage.setImageUrl(imageurl);

            tvName2.setText(itemname);
            tvPrice2.setText(itemprice);
            tvCategory2.setText(itemcategory);
            tvLocation2.setText(itemlocation);
            tvBrand2.setText(itembrand);
            tvQuantity2.setText(quantity);
            tvDescription2.setText(itemdiscreption);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void toast(String str) {
        Toast.makeText(getApplicationContext(), str, Toast.LENGTH_LONG).show();
    }
}
