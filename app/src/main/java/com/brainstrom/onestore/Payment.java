package com.brainstrom.onestore;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.brainstrom.onestore.adapter.MenuListAdapter;
import com.brainstrom.onestore.extras.Contents;
import com.brainstrom.onestore.extras.QRCodeEncoder;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;


public class Payment extends Activity implements AdapterView.OnItemClickListener {

    WebView wvList;
    String items, listid, paymentId = "";
    ListView lvActions;
    LinearLayout llLoad;
    int qrcodeOrBarcodeOrNfc; // qr code = 0 barcode = 1 and NFC = 2
    String _baseurl, name, email, username, bankid;
    ArrayList<String> ListName = new ArrayList<String>();
    ArrayList<String> ListSub = new ArrayList<String>();
    ArrayList<String> ListSign = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        getActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.color_actionbar)));
        wvList = (WebView) findViewById(R.id.wvPayment);
        lvActions = (ListView) findViewById(R.id.lvPaymentActions);
        llLoad = (LinearLayout) findViewById(R.id.llLoading);

        if (getIntent().hasExtra("items")) {
            items = getIntent().getStringExtra("items");
            listid = getIntent().getStringExtra("listid");
            wvList.loadData(items, "text/html", "UTF-8");
        }

        setMethods();
        lvActions.setAdapter(new MenuListAdapter(getApplicationContext(),
                ListName, ListSub, ListSign));

        lvActions.setOnItemClickListener(this);
    }

    public void setMethods() {
        ListName.add("Online Payment");
        ListSub.add("Fastest Method if you prefer");
        ListSign.add("1");

        ListName.add("Share QR Code");
        ListSub.add("Quick Response Is Great");
        ListSign.add("2");

        ListName.add("Share Barcode");
        ListSub.add("Simple Barcodes");
        ListSign.add("3");

        ListName.add("Via NFC");
        ListSub.add("Near Field Communication");
        ListSign.add("4");
    }

    @SuppressLint("NewApi")
    private void createQRCode(String qrInputText) {
        try {
            // Find screen size
            WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
            Display display = manager.getDefaultDisplay();
            Point point = new Point();
            display.getSize(point);
            int width = point.x;
            int height = point.y;
            int smallerDimension = width < height ? width : height;
            smallerDimension = smallerDimension * 3 / 4;
            // Encode with a QR Code image
            String barcodeFormat = "";
            if (qrcodeOrBarcodeOrNfc == 0) {
                barcodeFormat = BarcodeFormat.QR_CODE.toString();
            } else if (qrcodeOrBarcodeOrNfc == 1) {
                barcodeFormat = BarcodeFormat.CODE_128.toString();
            }
            QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(qrInputText, null,
                    Contents.Type.TEXT, barcodeFormat,
                    smallerDimension);
            Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();
            AlertDialog.Builder ad = new AlertDialog.Builder(this);
            ad.setCancelable(false);
            ImageView myImage = new ImageView(getApplicationContext());
            myImage.setImageBitmap(bitmap);
            ad.setView(myImage);
            ad.setPositiveButton("Close", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            ad.setNegativeButton("Create new", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getData("payment/payment.send.php?apikey=tejpratap&fromid=" + bankid + "&toid=0000000000&balance=" + getTotal());
                }
            });
            ad.show();
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    private String getTotal() {
        Document doc = Jsoup.parse(items);
        Elements tds = doc.getElementsByTag("td");
        return tds.get(tds.size() - 2).text();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        SharedPreferences pref = getSharedPreferences("oneStorePref", 0);
        name = pref.getString("name", "");
        username = pref.getString("username", "");
        email = pref.getString("email", "");
        bankid = pref.getString("bankid", "");

        if (username.length() > 1) {
            switch (i) {
                case 0:
                    getData("bank/bank.transfer.php?apikey=tejpratap&frombankid=" + bankid + "&tobankid=0000000000&balance=" + getTotal() + "&listid=" +listid);
                    break;
                case 1:
                    qrcodeOrBarcodeOrNfc = 0;
                    if (paymentId.length() == 0) {
                        getData("payment/payment.send.php?apikey=tejpratap&fromid=" + bankid + "&toid=0000000000&balance=" + getTotal() + "&listid=" +listid);
                    } else {
                        createQRCode(paymentId);
                    }
                    break;
                case 2:
                    qrcodeOrBarcodeOrNfc = 1;
                    if (paymentId.length() == 0) {
                        getData("payment/payment.send.php?apikey=tejpratap&fromid=" + bankid + "&toid=0000000000&balance=" + getTotal() + "&listid=" +listid);
                    } else {
                        createQRCode(paymentId);
                    }
                    break;
                case 3:
                    if (paymentId.length() == 0) {
                        getData("payment/payment.send.php?apikey=tejpratap&fromid=" + bankid + "&toid=0000000000&balance=" + getTotal() + "&listid=" +listid);
                        qrcodeOrBarcodeOrNfc = 2;
                    } else {
                        startActivity(new Intent(getApplicationContext(), NfcBeam.class).putExtra("id", paymentId));
                    }
                    break;
            }
        } else {
            toast("You need to login first");
        }
    }

    private void getBaseUrl() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "oneStorePref", 0);
        _baseurl = pref.getString("base_url",
                getResources().getString(R.string.base_url));
    }

    private void getData(final String Url) {
        getBaseUrl();
        String url = _baseurl + Url;
        llLoad.setVisibility(View.VISIBLE);
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // TODO Auto-generated method stub
                Log.d("Payment Url", Url);
                Log.d("Payment Response", response);
                try {
                    if (!new JSONObject(response).has("error")) {
                        if (Url.contains("bank.transfer.php")) {
                            String data = "Thank You, Your Payment is Completed";
                            try {
                                JSONObject main = new JSONObject(response);
                                data += "\nBalance Transfer : " + main.getString("balance") + "\nNew Balance : " + main.getString("frombankbalance");
                                showAck(true, data);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (Url.contains("payment.send.php")) {
                            try {
                                String id = new JSONObject(response).getString("id");
                                paymentId = id;
                                if (qrcodeOrBarcodeOrNfc == 0) {
                                    createQRCode(id);
                                } else if (qrcodeOrBarcodeOrNfc == 1) {
                                    createQRCode(id);
                                } else {
                                    startActivity(new Intent(getApplicationContext(), NfcBeam.class).putExtra("id", id));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        String error = "Error " + new JSONObject(response).getString("error");
                        toast(error);
                        showAck(false, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                llLoad.setVisibility(View.GONE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                toast("No internet connection");
                llLoad.setVisibility(View.GONE);
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }


    private void toast(String str) {
        Toast.makeText(getApplicationContext(), str, Toast.LENGTH_LONG).show();
    }

    /*
    * Show ack weather payment is completed or not
    * */
    private void showAck(boolean status, String reason) {
        AlertDialog.Builder ad = new AlertDialog.Builder(Payment.this);
        ad.setCancelable(false);
        if (status) {
            ad.setTitle("Payment Complete");
            ad.setMessage(reason);
        } else {
            ad.setTitle("Payment failed");
            ad.setMessage(reason);
        }
        ad.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        ad.show();
    }

    //    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_payment, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
}
