package com.brainstrom.onestore;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;

public class Login extends Activity implements OnClickListener {
    String _baseurl;
    GoogleCloudMessaging gcm;
    String regid;
    String PROJECT_NUMBER = "270571231689";
    LinearLayout llLoad, llLoginWindow, llRegisterWindow;
    Button bLogin, bRegister;
    EditText etLoginUsername, etLoginPassword, etRegisterName, etRegisterEmail,
            etRegisterUsername, etRegisterPassword, etRegisterBankid;
    boolean load = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        llLoginWindow = (LinearLayout) findViewById(R.id.llLoginLoginWindow);
        llRegisterWindow = (LinearLayout) findViewById(R.id.llLoginRegisterWindow);
        llLoad = (LinearLayout) findViewById(R.id.llLoading);
        bLogin = (Button) findViewById(R.id.bLoginLogin);
        bRegister = (Button) findViewById(R.id.bLoginRegister);
        etLoginUsername = (EditText) findViewById(R.id.etLoginLoginUsername);
        etLoginPassword = (EditText) findViewById(R.id.etLoginLoginPassword);
        etRegisterName = (EditText) findViewById(R.id.etLoginRegisterName);
        etRegisterEmail = (EditText) findViewById(R.id.etLoginRegisterEmail);
        etRegisterUsername = (EditText) findViewById(R.id.etLoginRegisterUsername);
        etRegisterPassword = (EditText) findViewById(R.id.etLoginRegisterPassword);
        etRegisterBankid = (EditText) findViewById(R.id.etLoginRegisterBankId);

        ActionBar ab = getActionBar();
        ab.setTitle("One Store");
        ab.setBackgroundDrawable(new ColorDrawable(getResources().getColor(
                R.color.color_actionbar)));

        if (getIntent().hasExtra("task")) {
            String task = getIntent().getStringExtra("task");
            if (task.contains("login")) {
                llLoginWindow.setVisibility(View.VISIBLE);
                llRegisterWindow.setVisibility(View.GONE);
            } else {
                llLoginWindow.setVisibility(View.GONE);
                llRegisterWindow.setVisibility(View.VISIBLE);
            }
        }

        getRegId();

        bLogin.setOnClickListener(this);
        bRegister.setOnClickListener(this);
    }

    private void getBaseUrl() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "oneStorePref", 0);
        _baseurl = pref.getString("base_url",
                getResources().getString(R.string.base_url));
    }

    private void getData(String urlAppend) {
        final String url = _baseurl + urlAppend;
        load = true;
        Log.d("url", url);
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // TODO Auto-generated method stub
                if (url.contains("customer.login.php")) {
                    login(response);
                } else {
                    register(response);
                    Log.d("response", response);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                toast("No internet connection");
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void register(String response) {
        // TODO Auto-generated method stub
        try {
            JSONObject main = new JSONObject(response);
            int status = main.getInt("status");
            if (status == 1) {
                String name = main.getString("name");
                String username = main.getString("username");
                String email = main.getString("email");
                String bankid = main.getString("bankid");

                toast("Welcome : " + name);

                SharedPreferences pref = getApplicationContext()
                        .getSharedPreferences("oneStorePref", 0);
                Editor edit = pref.edit();
                edit.putString("name", name);
                edit.putString("username", username);
                edit.putString("email", email);
                edit.putString("bankid", bankid);
                edit.commit();
                finish();
            } else {
                toast("Error : " + main.getString("error"));
                YoYo.with(Techniques.Tada).duration(500).delay(100)
                        .playOn(llRegisterWindow);
            }
            llLoad.setVisibility(View.GONE);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void getRegId() {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                    }

                    regid = gcm.register(PROJECT_NUMBER);
                    msg = "Device registered, registration ID=" + regid;
                    Log.i("GCM", "!!!!! " + regid);

                } catch (IOException ex) {
                    msg = "Error: " + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                Log.d("GCM ID", msg);
            }
        }.execute(null, null, null);
    }

    private void login(String response) {
        // TODO Auto-generated method stub
        try {
            JSONObject main = new JSONObject(response);
            int status = main.getInt("status");
            if (status == 1) {
                String name = main.getString("name");
                String username = main.getString("username");
                String email = main.getString("email");
                String bankid = main.getString("bankid");

                toast("Welcome : " + name);

                SharedPreferences pref = getApplicationContext()
                        .getSharedPreferences("oneStorePref", 0);
                Editor edit = pref.edit();
                edit.putString("name", name);
                edit.putString("username", username);
                edit.putString("email", email);
                edit.putString("bankid", bankid);
                edit.commit();
                finish();
            } else {
                toast("Error : " + main.getString("error"));
                YoYo.with(Techniques.Tada).duration(500).delay(100)
                        .playOn(llLoginWindow);
            }
            llLoad.setVisibility(View.GONE);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View arg0) {
        // TODO Auto-generated method stub
        switch (arg0.getId()) {
            case R.id.bLoginLogin:
                getBaseUrl();
                String urlLogin = "/customer/customer.login.php?apikey=tejpratap";
                urlLogin += "&username="
                        + etLoginUsername.getText().toString().replace(" ", "+");
                urlLogin += "&password=" + etLoginPassword.getText();
                getData(urlLogin);
                llLoad.setVisibility(View.VISIBLE);
                break;
            case R.id.bLoginRegister:
                getBaseUrl();
                String urlRegister = "/customer/customer.register.php?apikey=tejpratap";
                urlRegister += "&name="
                        + etRegisterName.getText();
                urlRegister += "&email=" + etRegisterEmail.getText();
                urlRegister += "&username=" + etRegisterUsername.getText();
                urlRegister += "&password=" + etRegisterPassword.getText();
                urlRegister += "&bankid=" + etRegisterBankid.getText();
                urlRegister += "&gcmid=" + regid;
                getData(urlRegister.replace(" ", "+"));
                llLoad.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void toast(String str) {
        Toast.makeText(getApplicationContext(), str, Toast.LENGTH_LONG).show();
    }
}
