package com.brainstrom.onestore;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.brainstrom.onestore.adapter.ListViewAdapterForLists;
import com.brainstrom.onestore.views.BounceListener;
import com.brainstrom.onestore.views.BounceScroller;
import com.brainstrom.onestore.views.BounceScroller.State;
import com.daimajia.swipe.implments.SwipeItemMangerImpl;

public class Lists extends Activity {

	private ListView mListView;
	private ListViewAdapterForLists mAdapter;
	private Context mContext = this;
	ArrayList<String> aListId = new ArrayList<String>();
	ArrayList<String> aListItems = new ArrayList<String>();
	String _baseurl, username;
	LinearLayout llLoad;
	TextView header;
	private BounceScroller scroller;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lists);
		mListView = (ListView) findViewById(R.id.lvLists);
		llLoad = (LinearLayout) findViewById(R.id.llLoading);

		ActionBar ab = getActionBar();
		ab.setTitle("One Store");
		ab.setBackgroundDrawable(new ColorDrawable(getResources().getColor(
				R.color.color_actionbar)));

		mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
//				Toast.makeText(mContext, "Double Click To View, Slide to Edit",
//						Toast.LENGTH_SHORT).show();
				Intent i = new Intent(mContext, ViewList.class);
				i.putExtra("listid", aListId.get(position));
				i.putExtra("items", aListItems.get(position));
				mContext.startActivity(i);
			}
		});
		getData();

		// Pull Down Scroller
		scroller = (BounceScroller) findViewById(R.id.bsLists);
		scroller.setListener(bl);
		scroller.ifHeaderBounce(true);
		scroller.ifFooterBounce(false);
		setHeaderView();

		header.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				getData();
				scroller.fitContent();
			}
		});
	}

	private void clear() {
		aListId.clear();
		aListItems.clear();
	}

	private void getBaseUrl() {
		SharedPreferences pref = getApplicationContext().getSharedPreferences(
				"oneStorePref", 0);
		_baseurl = pref.getString("base_url",
				getResources().getString(R.string.base_url));
		username = pref.getString("username", "");
	}

	private void getData() {
		getBaseUrl();
		clear();
		llLoad.setVisibility(View.VISIBLE);
		String url = _baseurl
				+ "/lists/lists.listsby.php?apikey=tejpratap&username="
				+ username;
		// Instantiate the RequestQueue.
		RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

		// Request a string response from the provided URL.
		StringRequest stringRequest = new StringRequest(Request.Method.GET,
				url, new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						// TODO Auto-generated method stub
						setList(response);
						llLoad.setVisibility(View.GONE);
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						toast("Cannot Connect Right Now");
                        llLoad.setVisibility(View.GONE);
					}
				});
		// Add the request to the RequestQueue.
		queue.add(stringRequest);
	}

	private void setList(String response) {
		// TODO Auto-generated method stub
		try {
			JSONObject main = new JSONObject(response);
			int status = main.getInt("status");
			if (status == 1) {
				JSONArray jar = main.getJSONArray("lists");
				for (int i = 0; i < jar.length(); i++) {
					JSONObject list = jar.getJSONObject(i);
					aListId.add(list.getString("listid"));
					String items = list.getString("items");
					Document doc = Jsoup.parse(items);
					Elements ids = doc.getElementsByTag("id");
					Elements names = doc.getElementsByTag("name");
					Elements qtys = doc.getElementsByTag("quantity");
					Elements costs = doc.getElementsByTag("cost");

					String itemlist = "<table style='width:100%;' >";
					itemlist = itemlist
							+ "<tr><td>name</td><td>price</td><td>qty</td></tr>";
					int total = 0;
					for (int j = 0; j < ids.size(); j++) {
						itemlist = itemlist + "<tr>";
						itemlist = itemlist + "<td>" + names.get(j).text()
								+ "</td>" + "<td>" + costs.get(j).text()
								+ "</td>" + "<td>" + qtys.get(j).text()
								+ "</td>";
						itemlist = itemlist + "</tr>";

						total = total
								+ (Integer.parseInt(costs.get(j).text()) * Integer
										.parseInt(qtys.get(j).text()));
					}
					itemlist = itemlist + "<td>total : </td>" + "<td>"
							+ Integer.toString(total) + "</td>" + "<td></td>";
					itemlist = itemlist + "</table>";

					// Log.d("ids", Integer.toString(ids.size()));
					// Log.d("names", Integer.toString(names.size()));
					// Log.d("qtys", Integer.toString(qtys.size()));
					// Log.d("costs", Integer.toString(costs.size()));
					aListItems.add(itemlist);
				}
				mAdapter = new ListViewAdapterForLists(this, aListId,
						aListItems);
				mListView.setAdapter(mAdapter);
				mAdapter.setMode(SwipeItemMangerImpl.Mode.Single);
			} else {
				toast("Error " + main.getString("error"));
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void toast(String str) {
		Toast.makeText(getApplicationContext(), str, Toast.LENGTH_SHORT).show();
	}

	// Pull Down Scroller
	public void setHeaderView() {
		header = new TextView(getApplicationContext());
		header.setPadding(20, 20, 20, 20);
		header.setText("Tap Here To Refresh");
		header.setBackgroundResource(R.drawable.fb_card);
		header.setTextColor(getResources().getColor(
				R.color.color_background_splash));
		header.setGravity(Gravity.CENTER);
		scroller.setHeaderView(header);
	}

	private BounceListener bl = new BounceListener() {
		@Override
		public void onState(boolean header, State state) {
			if (state == State.STATE_FIT_EXTRAS) {
				scroller.postDelayed(new Runnable() {
					@Override
					public void run() {
						scroller.fitContent();
					}
				}, 1500);
			}
		}

		@Override
		public void onOffset(boolean header, int offset) {
			// String position = header ? "header" : "footer";
			// Log.d("Offset", position + " offset " + offset);
		}
	};

	// @Override
	// public boolean onCreateOptionsMenu(Menu menu) {
	// // Inflate the menu; this adds items to the action bar if it is present.
	// getMenuInflater().inflate(R.menu.lists, menu);
	// return true;
	// }
}
