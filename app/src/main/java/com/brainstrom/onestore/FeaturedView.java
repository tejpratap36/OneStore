package com.brainstrom.onestore;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.brainstrom.onestore.adapter.GridViewItemAdapter;

public class FeaturedView extends Activity {

	GridView gvShow;
	String _baseurl, code;
	LinearLayout llLoad;
	TextView tvCode;
	ArrayList<String> aListId = new ArrayList<String>();
	ArrayList<String> aListName = new ArrayList<String>();
	ArrayList<String> aListPrice = new ArrayList<String>();
	ArrayList<String> aListImage = new ArrayList<String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_featured_view);
        getActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.color_actionbar)));
		gvShow = (GridView) findViewById(R.id.gvFeaturedShow);
		llLoad = (LinearLayout) findViewById(R.id.llLoading);
		tvCode = (TextView) findViewById(R.id.tvFeaturedShowCode);

		if (getIntent().hasExtra("code")) {
			code = getIntent().getStringExtra("code");
			tvCode.setText("Use Coupon Code '" + code
					+ "' At The Time Of Purchase");
			getData("/featured/featured.show.php?apikey=tejpratap&code=" + code);
			llLoad.setVisibility(View.VISIBLE);
		} else {
			toast("Error, go back and try again!");
		}

		gvShow.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getApplicationContext(), ViewItem.class);
				i.putExtra("id", aListId.get(arg2));
				startActivity(i);
			}
		});
	}

	// @Override
	// public boolean onCreateOptionsMenu(Menu menu) {
	// // Inflate the menu; this adds items to the action bar if it is present.
	// getMenuInflater().inflate(R.menu.featured_view, menu);
	// return true;
	// }

	private void getBaseUrl() {
		SharedPreferences pref = getApplicationContext().getSharedPreferences(
				"oneStorePref", 0);
		_baseurl = pref.getString("base_url",
				getResources().getString(R.string.base_url));
	}

	private void getData(final String Url) {
		getBaseUrl();
		String url = _baseurl + Url;
		// Instantiate the RequestQueue.
		RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

		// Request a string response from the provided URL.
		StringRequest stringRequest = new StringRequest(Request.Method.GET,
				url, new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						// TODO Auto-generated method stub
						setData(response);
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						toast("No internet connection");
					}
				});
		// Add the request to the RequestQueue.
		queue.add(stringRequest);
	}

	private void setData(String response) {
		// TODO Auto-generated method stub
		try {
			JSONObject main = new JSONObject(response);
			int status = main.getInt("status");
			if (status == 1) {
				JSONArray jar = main.getJSONArray("items");
				for (int i = 0; i < jar.length(); i++) {
					JSONObject market = jar.getJSONObject(i);
					aListId.add(market.getString("itemid"));
					aListName.add(market.getString("itemname"));
					aListPrice.add(market.getString("itemprice"));
					aListImage.add(market.getString("imageurl"));
				}
				llLoad.setVisibility(View.GONE);
				gvShow.setAdapter(new GridViewItemAdapter(this, aListImage,
						aListId, aListName, aListPrice));
			} else {
				toast("Error " + main.getString("error"));
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void toast(String str) {
		Toast.makeText(getApplicationContext(), str, Toast.LENGTH_LONG).show();
	}
}
