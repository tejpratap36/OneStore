package com.brainstrom.onestore;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.brainstrom.onestore.adapter.MenuListAdapter;
import com.brainstrom.onestore.database.ShoppingListDatabaseHandler;
import com.brainstrom.onestore.database.ShoppingListFrame;

public class ViewList extends Activity implements OnItemClickListener {

	ListView lvActions;
	WebView wvList;
	TextView tvListId;
	LinearLayout llLoad;
    Button bCoupon;
	ArrayList<String> ListName = new ArrayList<String>();
	ArrayList<String> ListSub = new ArrayList<String>();
	ArrayList<String> ListSign = new ArrayList<String>();
	ArrayList<String> aListId = new ArrayList<String>();
	ArrayList<String> aListName = new ArrayList<String>();
	ArrayList<String> aListPrice = new ArrayList<String>();
	ArrayList<String> aListQty = new ArrayList<String>();
	String _baseurl;
	String items, listid;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_list);

		wvList = (WebView) findViewById(R.id.wvViewList);
		lvActions = (ListView) findViewById(R.id.lvViewListActions);
		tvListId = (TextView) findViewById(R.id.tvViewListId);
        bCoupon = (Button) findViewById(R.id.bViewListCoupon);
		llLoad = (LinearLayout) findViewById(R.id.llLoading);

		ActionBar ab = getActionBar();
		ab.setTitle("One Store");
		ab.setBackgroundDrawable(new ColorDrawable(getResources().getColor(
				R.color.color_actionbar)));

		setActions();
		lvActions.setAdapter(new MenuListAdapter(getApplicationContext(),
				ListName, ListSub, ListSign));
		if (getIntent().hasExtra("listid")) {
			listid = getIntent().getStringExtra("listid");
		}
		if (getIntent().hasExtra("items")) {
			items = getIntent().getStringExtra("items");
			items = items
					+ "<style>table,th,td{border:1px solid black;border-collapse: collapse;}th,td{padding: 5px;}th{text-align: left;}</style>";
			showList(items);
            Log.d("ViewList", "has items");
		} else {
			getData("/lists/lists.show.php?apikey=tejpratap&listid=" + listid);
		}
		tvListId.setText("List Id : " + listid);
		lvActions.setOnItemClickListener(this);
        bCoupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData("/lists/lists.check.php?apikey=tejpratap&listid=" + listid);
            }
        });
	}

	private void showList(String items) {
		wvList.loadData(items, "text/html", "UTF-8");
	}

	@SuppressWarnings("deprecation")
	private void setActions() {
		// TODO Auto-generated method stub
		ListName.add("Get Map For List");
		ListSub.add("Map For Shopping As Fast as Possible.");
		ListSign.add("1");
		ListName.add("Next Step");
		ListSub.add("Check if items in your list are still available in market.");
		ListSign.add("2");
//		ListName.add("Add This List To My Shopping List");
//		ListSub.add("All items in this list will be added to your shopping list.");
//		ListSign.add("3");
//		ListName.add("Use As My Shopping List");
//		ListSub.add("All items in your shopping list will be replaced by this list.");
//		ListSign.add("4");
		ListName.add("Share This List");
		ListSub.add("Share list to someone who can shop on your behalf.");
		ListSign.add("3");
		ListName.add("Make List As Current");
		ListSub.add("It will be the first list in all your lists.");
		ListSign.add("4");

		if (Integer.valueOf(android.os.Build.VERSION.SDK) > 18) {
			ListName.add("Print List");
			ListSub.add("Print This List With Your Cloud Printer");
			ListSign.add("5");
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		SharedPreferences pref = getApplicationContext().getSharedPreferences(
				"oneStorePref", 0);
		switch (arg2) {
		case 0:
			Intent map = new Intent(ViewList.this, Map.class);
			map.putExtra("listid", listid);
			startActivity(map);
			break;
		case 1:
			getData("/lists/lists.check.php?apikey=tejpratap&listid=" + listid);
			break;
//		case 2:
//			makeList(items);
//			for (int i = 0; i < aListId.size(); i++) {
//				addToShopping(i);
//			}
//			toast("All items added to your shopping list");
//			break;
//		case 3:
//			deleteShopping();
//			makeList(items);
//			for (int i = 0; i < aListId.size(); i++) {
//				addToShopping(i);
//			}
//			toast("All items added to your shopping list");
//			break;
		case 2:
			showShareDialog();
			break;
		case 3:
			getData("/lists/lists.setcurrent.php?apikey=tejpratap&username="
					+ pref.getString("username", "") + "&listid=" + listid);
			break;
		case 4:
			print(wvList);
			break;
		default:
			break;
		}
	}

	private void showShareDialog() {
		AlertDialog.Builder ad = new AlertDialog.Builder(this);
		ad.setTitle("Share");
		ad.setMessage("Enter username of customer with whome you want to share this list");
		final EditText et = new EditText(this);
		et.setHint("username");
		ad.setView(et);
		ad.setPositiveButton("Share", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				// TODO Auto-generated method stub
				getData("/lists/lists.share.php?apikey=tejpratap&customerid="
						+ et.getText().toString() + "&listid=" + listid);
			}
		});
		ad.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		ad.show();
	}

	private void print(WebView webView) {
		try {
			// PrintManager
			String PRINT_SERVICE = (String) Context.class.getDeclaredField(
					"PRINT_SERVICE").get(null);
			Object printManager = this.getSystemService(PRINT_SERVICE);

			// PrintDocumentAdapter
			Class<?> printDocumentAdapterClass = Class
					.forName("android.print.PrintDocumentAdapter");
			Method createPrintDocumentAdapterMethod = webView.getClass()
					.getMethod("createPrintDocumentAdapter");
			Object printAdapter = createPrintDocumentAdapterMethod
					.invoke(webView);

			// PrintAttributes
			Class<?> printAttributesBuilderClass = Class
					.forName("android.print.PrintAttributes$Builder");
			Constructor<?> ctor = printAttributesBuilderClass.getConstructor();
			Object printAttributes = ctor.newInstance(new Object[] {});
			Method buildMethod = printAttributes.getClass().getMethod("build");
			Object printAttributesBuild = buildMethod.invoke(printAttributes);

			// PrintJob
			String jobName = "My Document";
			Method printMethod = printManager.getClass().getMethod("print",
					String.class, printDocumentAdapterClass,
					printAttributesBuild.getClass());
			Object printJob = printMethod.invoke(printManager, jobName,
					printAdapter, printAttributesBuild);
		} catch (Exception e) {

		}
	}

	private void getBaseUrl() {
		SharedPreferences pref = getApplicationContext().getSharedPreferences(
				"oneStorePref", 0);
		_baseurl = pref.getString("base_url",
				getResources().getString(R.string.base_url));
	}

	private void getData(final String Url) {
		getBaseUrl();
		String url = _baseurl + Url;
		llLoad.setVisibility(View.VISIBLE);
		// Instantiate the RequestQueue.
		RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

		// Request a string response from the provided URL.
		StringRequest stringRequest = new StringRequest(Request.Method.GET,
				url, new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						// TODO Auto-generated method stub
						if (Url.contains("lists.show.php")) {
							parseList(response);
						} else if (Url.contains("lists.setcurrent.php")) {
							setCurrent(response);
						} else if (Url.contains("lists.share.php")) {
							setShare(response);
						} else if (Url.contains("lists.check.php")) {
							setCheck(response);
						}
						llLoad.setVisibility(View.GONE);
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						toast("No internet connection");
                        llLoad.setVisibility(View.GONE);
					}
				});
		// Add the request to the RequestQueue.
		queue.add(stringRequest);
	}

	private void parseList(String response) {
		try {
			JSONObject main = new JSONObject(response);
			int status = main.getInt("status");
			if (status == 1) {
				items = main.getString("items");
				String itemlist = tabularizeList(items);
				showList(itemlist);
			} else {
				toast("Error " + main.getString("error"));
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private String tabularizeList(String items) {
		Document doc = Jsoup.parse(items);
		Elements ids = doc.getElementsByTag("id");
		Elements names = doc.getElementsByTag("name");
		Elements qtys = doc.getElementsByTag("quantity");
		Elements costs = doc.getElementsByTag("cost");

		int total = 0;
		String itemlist = "<style>table,th,td{border:1px solid black;border-collapse: collapse;}th,td{padding: 5px;}th{text-align: left;}</style>";
		itemlist = "<table style='width:100%;' >";
		itemlist = itemlist
				+ "<tr><td>name</td><td>price</td><td>qty</td></tr>";
		for (int j = 0; j < ids.size(); j++) {
			itemlist = itemlist + "<tr>";
			itemlist = itemlist + "<td>" + names.get(j).text() + "</td>"
					+ "<td>" + costs.get(j).text() + "</td>" + "<td>"
					+ qtys.get(j).text() + "</td>";
			itemlist = itemlist + "</tr>";
			total = total
					+ (Integer.parseInt(costs.get(j).text()) * Integer
							.parseInt(qtys.get(j).text()));
		}
		itemlist = itemlist + "<td>total : </td>" + "<td>"
				+ Integer.toString(total) + "</td>" + "<td></td>";
		itemlist = itemlist + "</table>";
		return itemlist;
	}

	private void clear() {
		aListId.clear();
		aListName.clear();
		aListPrice.clear();
		aListQty.clear();
	}

	private void makeList(String items) {
		clear();
		Document doc = Jsoup.parse(items);
		Elements ids = doc.getElementsByTag("id");
		Elements names = doc.getElementsByTag("name");
		Elements qtys = doc.getElementsByTag("quantity");
		Elements costs = doc.getElementsByTag("cost");

		for (int i = 0; i < ids.size(); i++) {
			aListId.add(ids.get(i).text());
		}
		for (int i = 0; i < names.size(); i++) {
			aListName.add(names.get(i).text());
		}
		for (int i = 0; i < qtys.size(); i++) {
			aListQty.add(qtys.get(i).text());
		}
		for (int i = 0; i < costs.size(); i++) {
			aListPrice.add(costs.get(i).text());
		}

		Log.d("View item list id", Integer.toString(aListId.size()));
		Log.d("View item list name", Integer.toString(aListName.size()));
		Log.d("View item list qty", Integer.toString(aListQty.size()));
		Log.d("View item list price", Integer.toString(aListPrice.size()));
	}

	private void setCurrent(String response) {
		// TODO Auto-generated method stub
		try {
			JSONObject main = new JSONObject(response);
			int status = main.getInt("status");
			if (status == 1) {
				String lid = main.getString("listid");
				tvListId.setText("List Id : " + lid);
				toast("Now this list is set to current");
			} else {
				toast("Error " + main.getString("error"));
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void setShare(String response) {
		// TODO Auto-generated method stub
		try {
			JSONObject main = new JSONObject(response);
			int status = main.getInt("status");
			if (status == 1) {
				toast("List has been shared to user");
			} else {
				toast("Error " + main.getString("error"));
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void setCheck(String response) {
		// TODO Auto-generated method stub
		try {
			JSONObject main = new JSONObject(response);
			int status = main.getInt("status");
			if (status == 1) {
				String listok = main.getString("is_list_ok");
				if (listok.contains("yes")) {
					showDialogForCheck(true, "", "", "");
				} else {
					String itemids = "", itemnames = "";
					String itemcount = main.getString("count");
					JSONArray ids = main.getJSONArray("itemid");
					for (int i = 0; i < ids.length(); i++) {
						itemids = itemids + "\n" + ids.getString(i);
					}
					JSONArray names = main.getJSONArray("itemname");
					for (int i = 0; i < names.length(); i++) {
						itemnames = itemnames + "\n" + names.getString(i);
					}
					showDialogForCheck(false, itemcount, itemids, itemnames);
				}

			} else {
				toast("Error " + main.getString("error"));
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showDialogForCheck(boolean listok, String itemcount,
			String itemids, String itemnames) {
		AlertDialog.Builder ad = new AlertDialog.Builder(this);
        ad.setCancelable(false);
		if (listok) {
			ad.setTitle("Your List Is Ok");
			ad.setMessage("All item's in the list are available in market.");
            ad.setNeutralButton("Apply Coupon",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            Intent i = new Intent(getApplicationContext(),Coupon.class);
                            i.putExtra("items", items);
                            i.putExtra("listid", listid);
                            startActivity(i);
                        }
                    });
            ad.setPositiveButton("Payment",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            Intent i = new Intent(getApplicationContext(),Payment.class);
                            i.putExtra("items", items);
                            i.putExtra("listid", listid);
                            startActivity(i);
                        }
                    });
		} else {
			ad.setTitle("Your List Is Not Ok");
			ad.setMessage(itemcount
					+ " items are currently sold out of the market, The items are : \n"
					+ itemnames);
			ad.setPositiveButton("Auto Update List Now",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							getData("/lists/lists.check.php?apikey=tejpratap&update=true&listid="
									+ listid);
							getData("/lists/lists.show.php?apikey=tejpratap&listid="
									+ listid);
						}
					});
		}
		ad.setNegativeButton("Close", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		ad.show();
	}

	private void addToShopping(int pos) {
		try {
			int id = Integer.parseInt(aListId.get(pos));
			ShoppingListDatabaseHandler db = new ShoppingListDatabaseHandler(
					getApplicationContext());
			boolean has = db.Exists(id);
			if (has) {
				ShoppingListFrame sl = db.getshoppingList(id);
				db.updateshoppingList(new ShoppingListFrame(id, aListName
						.get(pos), aListPrice.get(pos), sl.getQuantity() + 1));

				Log.d("added2", "Id : " + id + " Name : " + aListName.get(pos)
						+ " Cost : " + aListPrice.get(pos) + " Quantity : "
						+ sl.getQuantity() + 1);
			} else {
				db.addshoppingList(new ShoppingListFrame(id,
						aListName.get(pos), aListPrice.get(pos), 1));
				Log.d("added3", "Id : " + id + " Name : " + aListName.get(pos)
						+ " Cost : " + aListPrice.get(pos) + " Quantity : " + 1);
			}
		} catch (Exception e) {
			toast("Error  : " + e.getMessage());
		}
	}

	private void deleteShopping() {
		ShoppingListDatabaseHandler db = new ShoppingListDatabaseHandler(
				getApplicationContext());
		db.deleteAll();
	}

	private void toast(String str) {
		Toast.makeText(getApplicationContext(), str, Toast.LENGTH_LONG).show();
	}

	// @Override
	// public boolean onCreateOptionsMenu(Menu menu) {
	// // Inflate the menu; this adds items to the action bar if it is present.
	// getMenuInflater().inflate(R.menu.view_list, menu);
	// return true;
	// }

}
