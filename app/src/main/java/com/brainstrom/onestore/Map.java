package com.brainstrom.onestore;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.swipe.SwipeLayout;

public class Map extends Activity {

	String _baseurl, listid;
	WebView WvMap;
	ListView lvPath;
	ArrayList<String> aListRoute = new ArrayList<String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		SwipeLayout swipeLayout = (SwipeLayout) findViewById(R.id.swipe);
		swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
		swipeLayout.setDragEdge(SwipeLayout.DragEdge.Bottom);
		WvMap = (WebView) findViewById(R.id.wvMapMap);
		lvPath = (ListView) findViewById(R.id.lvMapPath);
		
		ActionBar ab = getActionBar();
		ab.setTitle("One Store");
		ab.setBackgroundDrawable(new ColorDrawable(getResources().getColor(
				R.color.color_actionbar)));

		WvMap.getSettings().setBuiltInZoomControls(true);
		WvMap.getSettings().setSupportZoom(true);

		if (getIntent().hasExtra("listid")) {
			listid = getIntent().getStringExtra("listid");
		} else {
			listid = "152346954";
		}

		WvMap.loadUrl("http://s26.postimg.org/op44hne4p/mall_map.jpg");
		getData("/orientation/orientation.tsp.php?apikey=tejpratap&listid="
				+ listid);
	}

	// @Override
	// public boolean onCreateOptionsMenu(Menu menu) {
	// // Inflate the menu; this adds items to the action bar if it is present.
	// getMenuInflater().inflate(R.menu.map, menu);
	// return true;
	// }

	private void getBaseUrl() {
		SharedPreferences pref = getApplicationContext().getSharedPreferences(
				"oneStorePref", 0);
		_baseurl = pref.getString("base_url",
				getResources().getString(R.string.base_url));
	}

	private void getData(final String Url) {
		getBaseUrl();
		String url = _baseurl + Url;
		// Instantiate the RequestQueue.
		RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

		// Request a string response from the provided URL.
		StringRequest stringRequest = new StringRequest(Request.Method.GET,
				url, new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						// TODO Auto-generated method stub
						addRoute(response);
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						toast("No internet connection");
					}
				});
		// Add the request to the RequestQueue.
		queue.add(stringRequest);
	}

	private void addRoute(String response) {
		try {
			aListRoute.clear();
			JSONObject main = new JSONObject(response);
			JSONArray sort = main.getJSONArray("shortest_path");
			for (int i = 0; i < sort.length(); i++) {
				if (i == 0) {
					aListRoute.add("Start From Shop No. " + sort.getString(i));
				} else {
					aListRoute.add("Then Go To  Shop No. " + sort.getString(i));
				}
			}
			aListRoute.add("Now Proceed For Payments");
			lvPath.setAdapter(new ArrayAdapter<String>(getApplicationContext(),
					R.layout.single_row_map, R.id.tvMenuTitle, aListRoute));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void toast(String str) {
		Toast.makeText(getApplicationContext(), str, Toast.LENGTH_LONG).show();
	}
}
