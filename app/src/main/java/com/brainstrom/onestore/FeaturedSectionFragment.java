package com.brainstrom.onestore;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.brainstrom.onestore.adapter.BigImageAdapter;
import com.brainstrom.onestore.views.BounceListener;
import com.brainstrom.onestore.views.BounceScroller;
import com.brainstrom.onestore.views.BounceScroller.State;

public class FeaturedSectionFragment extends Fragment implements
		OnItemClickListener {

	String _baseurl;
	GridView gvFeatured;
	int status;
	TextView header;
	private BounceScroller scroller;
	LinearLayout llLoad;
	ArrayList<String> aListCode = new ArrayList<String>();
	ArrayList<String> aListImage = new ArrayList<String>();
	ArrayList<String> aListDescription = new ArrayList<String>();
	boolean load = false;

	public FeaturedSectionFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_home_featured,
				container, false);
		gvFeatured = (GridView) rootView.findViewById(R.id.gvHomeFeatured);
		llLoad = (LinearLayout) rootView.findViewById(R.id.llLoading);
		if (!load) {
			getBaseUrl();
			getData();
		} else {
			gvFeatured.setAdapter(new BigImageAdapter(getActivity(),
					aListImage, aListDescription));
		}

		gvFeatured.setOnItemClickListener(this);

		// Pull Down Scroller
		scroller = (BounceScroller) rootView.findViewById(R.id.pc_root);
		scroller.setListener(bl);
		scroller.ifHeaderBounce(true);
		scroller.ifFooterBounce(false);
		setHeaderView();

		header.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				getData();
				scroller.fitContent();
			}
		});
		
		return rootView;
	}

	private void clear() {
		aListCode.clear();
		aListImage.clear();
		aListDescription.clear();
	}

	private void getBaseUrl() {
		SharedPreferences pref = getActivity().getSharedPreferences(
				"oneStorePref", 0);
		_baseurl = pref.getString("base_url",
				getResources().getString(R.string.base_url));
	}

	private void getData() {
		String url = _baseurl
				+ "/featured/featured.showall.php?apikey=tejpratap";
		load = true;
		llLoad.setVisibility(View.VISIBLE);
		// Instantiate the RequestQueue.
		RequestQueue queue = Volley.newRequestQueue(getActivity());

		// Request a string response from the provided URL.
		StringRequest stringRequest = new StringRequest(Request.Method.GET,
				url, new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						// TODO Auto-generated method stub
						Log.d("Featured Section", response);
						setValues(response);
						llLoad.setVisibility(View.GONE);
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						toast("No internet connection");
						llLoad.setVisibility(View.GONE);
					}
				});
		// Add the request to the RequestQueue.
		queue.add(stringRequest);
	}

	private void setValues(String Response) {
		clear();

		try {
			JSONObject main = new JSONObject(Response);
			status = main.getInt("status");
			if (status == 1) {
				JSONArray jar = main.getJSONArray("featured");
				for (int i = 0; i < jar.length(); i++) {
					JSONObject featured = jar.getJSONObject(i);
					try {
						aListCode.add(featured.getString("code"));
					} catch (Exception e) {
					}
					try {
						aListImage.add(featured.getString("imageurl"));
					} catch (Exception e) {
					}
					try {
						aListDescription.add(featured.getString("description"));
					} catch (Exception e) {
					}
				}
			} else {
				toast(main.getString("error"));
			}
			llLoad.setVisibility(View.GONE);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
		}
		gvFeatured.setAdapter(new BigImageAdapter(getActivity(), aListImage,
				aListDescription));
	}

	// Pull Down Scroller
	public void setHeaderView() {
		header = new TextView(getActivity());
		header.setPadding(20, 20, 20, 20);
		header.setText("Tap Here To Refresh");
		header.setBackgroundResource(R.drawable.fb_card);
		header.setTextColor(getResources().getColor(
				R.color.color_background_splash));
		header.setGravity(Gravity.CENTER);
		scroller.setHeaderView(header);
	}

	private BounceListener bl = new BounceListener() {
		@Override
		public void onState(boolean header, State state) {
			if (state == State.STATE_FIT_EXTRAS) {
				scroller.postDelayed(new Runnable() {
					@Override
					public void run() {
						scroller.fitContent();
					}
				}, 1500);
			}
		}

		@Override
		public void onOffset(boolean header, int offset) {
			// String position = header ? "header" : "footer";
			// Log.d("Offset", position + " offset " + offset);
		}
	};

	private void toast(String str) {
		Toast.makeText(getActivity(), str, Toast.LENGTH_LONG).show();
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		Intent fe = new Intent(getActivity(), FeaturedView.class);
		fe.putExtra("code", aListCode.get(arg2));
		startActivity(fe);
	}
}