package com.brainstrom.onestore;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.brainstrom.onestore.adapter.GridViewItemAdapter;
import com.brainstrom.onestore.views.BounceListener;
import com.brainstrom.onestore.views.BounceScroller;
import com.brainstrom.onestore.views.BounceScroller.State;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

public class NewArrivalSectionFragment extends Fragment {

	String _baseurl;
	GridView gvFresh;
	int status, page;
	TextView header, footer;
	private BounceScroller scroller;
//SwipyRefreshLayout mSwipyRefreshLayout;
	LinearLayout llLoad;
	ArrayList<String> aListId = new ArrayList<String>();
	ArrayList<String> aListName = new ArrayList<String>();
	ArrayList<String> aListPrice = new ArrayList<String>();
	ArrayList<String> aListImage = new ArrayList<String>();
	ArrayList<String> aListNamePrice = new ArrayList<String>();
	boolean load = false;

	public NewArrivalSectionFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_grid, container,
				false);
		gvFresh = (GridView) rootView.findViewById(R.id.gvFragment);
//        mSwipyRefreshLayout = (SwipyRefreshLayout) rootView.findViewById(R.id.swipyrefreshlayout);
//        mSwipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
		llLoad = (LinearLayout) rootView.findViewById(R.id.llLoading);
		if (!load) {
			getBaseUrl();
			getData(1);
			llLoad.setVisibility(View.VISIBLE);
		} else {
			gvFresh.setAdapter(new GridViewItemAdapter(getActivity(),
					aListImage, aListId, aListName, aListPrice));
		}

		gvFresh.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getActivity(), ViewItem.class);
				i.putExtra("id", aListId.get(arg2));
				startActivity(i);
			}
		});

//        mSwipyRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh(SwipyRefreshLayoutDirection direction) {
//                Log.d("MainActivity", "Refresh triggered at "
//                        + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));
 //           }
//        });

//		Pull Down Scroller
		scroller = (BounceScroller) rootView.findViewById(R.id.pc_root);
		scroller.setListener(bl);
		scroller.ifHeaderBounce(true);
		scroller.ifFooterBounce(true);
		setHeaderView();
		setFooterView();

		header.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				page = 1;
				getData(page);
				llLoad.setVisibility(View.VISIBLE);
				scroller.fitContent();
			}
		});
		footer.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				getData(page++);
				llLoad.setVisibility(View.VISIBLE);
				scroller.fitContent();
			}
		});

		return rootView;
	}

	private void clear() {
		aListId.clear();
		aListName.clear();
		aListPrice.clear();
		aListImage.clear();
	}

	private void getBaseUrl() {
		SharedPreferences pref = getActivity().getSharedPreferences(
				"oneStorePref", 0);
		_baseurl = pref.getString("base_url",
				getResources().getString(R.string.base_url));
	}

	private void getData(int page) {
		String url = _baseurl
				+ "/market/market.fresh.php?apikey=tejpratap&page=" + page;
		load = true;
		// Instantiate the RequestQueue.
		RequestQueue queue = Volley.newRequestQueue(getActivity());

		// Request a string response from the provided URL.
		StringRequest stringRequest = new StringRequest(Request.Method.GET,
				url, new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						// TODO Auto-generated method stub
						setValues(response);
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						toast("No internet connection");
					}
				});
		// Add the request to the RequestQueue.
		queue.add(stringRequest);
	}

	private void setValues(String Response) {
		clear();

		try {
			JSONObject main = new JSONObject(Response);
			status = main.getInt("status");
			if (status == 1) {
				JSONArray jar = main.getJSONArray("market");
				for (int i = 0; i < jar.length(); i++) {
					JSONObject market = jar.getJSONObject(i);
					aListId.add(market.getString("itemid"));
					aListName.add(market.getString("itemname"));
					aListPrice.add(market.getString("itemprice"));
					aListImage.add(market.getString("imageurl"));
					aListNamePrice.add(market.getString("itemname") + " ("
							+ market.getString("itemprice") + ")");
				}
			} else {
				toast(main.getString("error"));
			}
			llLoad.setVisibility(View.GONE);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
		}
		gvFresh.setAdapter(new GridViewItemAdapter(getActivity(), aListImage,
				aListId, aListName, aListPrice));
		// gvFresh.setAdapter(new GridSingleItem(getActivity(), aListImage,
		// aListNamePrice));
	}

	private void toast(String str) {
		Toast.makeText(getActivity(), str, Toast.LENGTH_LONG).show();
	}

	// Pull Down Scroller
	public void setHeaderView() {
		header = new TextView(getActivity());
		header.setPadding(20, 20, 20, 20);
		header.setText("Tap Here To Refresh");
		header.setBackgroundResource(R.drawable.fb_card);
		header.setTextColor(getResources().getColor(
				R.color.color_background_splash));
		header.setGravity(Gravity.CENTER);
		scroller.setHeaderView(header);
	}

	public void setFooterView() {
		footer = new TextView(getActivity());
		footer.setPadding(20, 20, 20, 20);
		footer.setText("Tap Here To Load More");
		footer.setBackgroundResource(R.drawable.fb_card);
		footer.setTextColor(getResources().getColor(
				R.color.color_background_splash));
		footer.setGravity(Gravity.CENTER);
		scroller.setFooterView(footer);
	}

	private BounceListener bl = new BounceListener() {
		@Override
		public void onState(boolean header, State state) {
			if (state == State.STATE_FIT_EXTRAS) {
				scroller.postDelayed(new Runnable() {
					@Override
					public void run() {
						scroller.fitContent();
					}
				}, 500);
			}
		}

		@Override
		public void onOffset(boolean header, int offset) {
			// String position = header ? "header" : "footer";
			// Log.d("Offset", position + " offset " + offset);
		}
	};
}