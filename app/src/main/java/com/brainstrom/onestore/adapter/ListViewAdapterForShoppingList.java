package com.brainstrom.onestore.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.brainstrom.onestore.R;
import com.brainstrom.onestore.ViewItem;
import com.brainstrom.onestore.database.ShoppingListDatabaseHandler;
import com.brainstrom.onestore.database.ShoppingListFrame;
import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.BaseSwipeAdapter;

public class ListViewAdapterForShoppingList extends BaseSwipeAdapter {

	private Context mContext;
	ArrayList<String> aListid = new ArrayList<String>();
	ArrayList<String> aListname = new ArrayList<String>();
	ArrayList<String> aListcost = new ArrayList<String>();
	ArrayList<String> aListquantity = new ArrayList<String>();

	public ListViewAdapterForShoppingList(Context mContext) {
		this.mContext = mContext;
		load();
	}

	@Override
	public int getSwipeLayoutResourceId(int position) {
		return R.id.swipe;
	}

	@Override
	public View generateView(final int position, ViewGroup parent) {
		View v = LayoutInflater.from(mContext).inflate(R.layout.listview_item,
				null);
		final SwipeLayout swipeLayout = (SwipeLayout) v
				.findViewById(getSwipeLayoutResourceId(position));
		swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
		swipeLayout.setDragEdge(SwipeLayout.DragEdge.Right);
		Button delete = (Button) v.findViewById(R.id.bSwipeItemDelete);
		swipeLayout.addSwipeListener(new SimpleSwipeListener() {
			@Override
			public void onOpen(SwipeLayout layout) {
				// YoYo.with(Techniques.Tada).duration(500).delay(100).playOn(layout.findViewById(R.id.trash));
			}
		});
		swipeLayout
				.setOnDoubleClickListener(new SwipeLayout.DoubleClickListener() {
					@Override
					public void onDoubleClick(SwipeLayout layout,
							boolean surface) {
						Intent i = new Intent(mContext, ViewItem.class);
						i.putExtra("id", aListid.get(position));
						mContext.startActivity(i);
					}
				});
		delete.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				deleteItem(position);
				swipeLayout.setVisibility(View.GONE);
			}
		});
		return v;
	}

	@Override
	public void fillValues(int position, View convertView) {
		TextView name = (TextView) convertView
				.findViewById(R.id.tvSwipeItemName);
		TextView cost = (TextView) convertView
				.findViewById(R.id.tvSwipeItemCost);
		TextView quantity = (TextView) convertView
				.findViewById(R.id.tvSwipeItemQuantity);
		name.setText(aListname.get(position));
		cost.setText(aListcost.get(position));
		quantity.setText(aListquantity.get(position));
		Log.d("fillTest", Integer.toString(position));
	}

	@Override
	public int getCount() {
		Log.d("size", Integer.toString(aListname.size()));
		return aListname.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	private void deleteItem(int pos) {
		try {
			ShoppingListDatabaseHandler db = new ShoppingListDatabaseHandler(
					mContext);
			db.deleteshoppingList(new ShoppingListFrame(Integer
					.parseInt(aListid.get(pos)), aListname.get(pos), aListcost
					.get(pos), Integer.parseInt(aListquantity.get(pos))));
			Toast.makeText(mContext, aListname.get(pos) + " Is deleted",
					Toast.LENGTH_SHORT).show();
		} catch (Exception e) {

		}

	}

	private void load() {
		Log.d("Reading: ", "Reading all contacts..");
		ShoppingListDatabaseHandler db = new ShoppingListDatabaseHandler(
				mContext);
		List<ShoppingListFrame> shoppinglist = db.getAllshoppingLists();

		for (ShoppingListFrame cn : shoppinglist) {
			String log = "Id: " + cn.getID() + " ,Name: " + cn.getName()
					+ " ,price: " + cn.getPrice() + " ,quantity: "
					+ cn.getQuantity();
			// Writing Contacts to log
			Log.d("Name: ", log);

			aListid.add(Integer.toString(cn.getID()));
			aListname.add(cn.getName());
			aListcost.add(cn.getPrice());
			aListquantity.add(Integer.toString(cn.getQuantity()));
		}
	}
}
