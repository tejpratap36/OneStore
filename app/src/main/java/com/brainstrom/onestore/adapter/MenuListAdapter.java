package com.brainstrom.onestore.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.brainstrom.onestore.R;

public class MenuListAdapter extends BaseAdapter {
	ArrayList<singlerowMenu> list;

	Context context;

	public MenuListAdapter(Context c, ArrayList<String> arraylistname,
			ArrayList<String> arraylistsub, ArrayList<String> arraylistsign) {
		context = c;
		list = new ArrayList<singlerowMenu>();

		for (int i = 0; i < arraylistname.size(); i++) {
			list.add(new singlerowMenu(arraylistname.get(i), arraylistsub
					.get(i), arraylistsign.get(i)));
		}
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return list.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		LayoutInflater inflator = (LayoutInflater) context
				.getSystemService(context.LAYOUT_INFLATER_SERVICE);
		View row = inflator.inflate(R.layout.single_row_transparent_menu, arg2, false);
		TextView title = (TextView) row.findViewById(R.id.tvListTextSubTitle);
		TextView sub = (TextView) row.findViewById(R.id.tvListTextSubTagMenu);
		TextView sign = (TextView) row.findViewById(R.id.tvListTextCountMenu);

		singlerowMenu temp = list.get(arg0);
		title.setText(temp.title);
		sub.setText(temp.sub);
		sign.setText(temp.sign);
		return row;
	}
}

class singlerowMenu {
	String title, sub, sign;

	singlerowMenu(String title, String sub, String sign) {
		this.title = title;
		this.sub = sub;
		this.sign = sign;
	}
}