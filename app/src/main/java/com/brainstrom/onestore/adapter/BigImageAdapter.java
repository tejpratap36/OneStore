package com.brainstrom.onestore.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.brainstrom.onestore.R;
import com.loopj.android.image.SmartImageView;

public class BigImageAdapter extends BaseAdapter {
	ArrayList<singlerow> list;

	Context context;

	public BigImageAdapter(Context c, ArrayList<String> arraylistImage,
			ArrayList<String> arraylistText) {
		context = c;
		list = new ArrayList<singlerow>();

		for (int i = 0; i < arraylistImage.size(); i++) {
			list.add(new singlerow(arraylistImage.get(i), arraylistText.get(i)));
		}
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return list.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		LayoutInflater inflator = (LayoutInflater) context
				.getSystemService(context.LAYOUT_INFLATER_SERVICE);
		View row = inflator
				.inflate(R.layout.single_grid_big_image, arg2, false);
		TextView title = (TextView) row.findViewById(R.id.tvSingleGridBigImage);
		SmartImageView img = (SmartImageView) row
				.findViewById(R.id.ivSingleGridBigImage);

		singlerow temp = list.get(arg0);
		title.setText(temp.title);
		img.setImageUrl(temp.image);
		return row;
	}

}

class singlerow {
	String title, image;

	singlerow(String image, String title) {
		this.title = title;
		this.image = image;
	}
}