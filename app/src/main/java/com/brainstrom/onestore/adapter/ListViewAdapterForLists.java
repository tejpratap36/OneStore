package com.brainstrom.onestore.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.brainstrom.onestore.R;
import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.BaseSwipeAdapter;

public class ListViewAdapterForLists extends BaseSwipeAdapter {

	private Context mContext;
	String _baseurl;
	ArrayList<String> aListid = new ArrayList<String>();
	ArrayList<String> aListitems = new ArrayList<String>();

	public ListViewAdapterForLists(Context mContext, ArrayList<String> id,
			ArrayList<String> items) {
		this.mContext = mContext;
		aListid = id;
		aListitems = items;
	}

	@Override
	public int getSwipeLayoutResourceId(int position) {
		return R.id.swipe;
	}

	@Override
	public View generateView(final int position, ViewGroup parent) {
		View v = LayoutInflater.from(mContext).inflate(
				R.layout.listview_item_for_lists, null);
		final SwipeLayout swipeLayout = (SwipeLayout) v
				.findViewById(getSwipeLayoutResourceId(position));
		swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
		swipeLayout.setDragEdge(SwipeLayout.DragEdge.Right);
		Button delete = (Button) v.findViewById(R.id.bSwipeItemDelete);
		swipeLayout.addSwipeListener(new SimpleSwipeListener() {
			@Override
			public void onOpen(SwipeLayout layout) {
				// YoYo.with(Techniques.Tada).duration(500).delay(100).playOn(layout.findViewById(R.id.trash));
			}
		});
		swipeLayout
				.setOnDoubleClickListener(new SwipeLayout.DoubleClickListener() {
					@Override
					public void onDoubleClick(SwipeLayout layout,
							boolean surface) {
						// Intent i = new Intent(mContext, ViewList.class);
						// i.putExtra("listid", aListid.get(position));
						// i.putExtra("items", aListitems.get(position));
						// mContext.startActivity(i);
						// toast(aListid.get(position));
						// toast(aListitems.get(position));
					}
				});
		delete.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				deleteItem(position);
				swipeLayout.setVisibility(View.GONE);
			}
		});
		return v;
	}

	@Override
	public void fillValues(int position, View convertView) {
		TextView id = (TextView) convertView.findViewById(R.id.tvSwipeItemId);
		WebView items = (WebView) convertView
				.findViewById(R.id.wvSwipeItemItems);
		id.setText(aListid.get(position));
		items.loadData(aListitems.get(position), "text/html", "UTF-8");
	}

	@Override
	public int getCount() {
		return aListid.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	private void deleteItem(int pos) {
		getData("/lists/lists.delete.php?apikey=tejpratap&listid="
				+ aListid.get(pos));
	}

	private void getBaseUrl() {
		SharedPreferences pref = mContext.getSharedPreferences("oneStorePref",
				0);
		_baseurl = pref.getString("base_url", mContext.getResources()
				.getString(R.string.base_url));
	}

	private void getData(final String Url) {
		getBaseUrl();
		RequestQueue queue = Volley.newRequestQueue(mContext);
		String url = _baseurl + Url;
		// Request a string response from the provided URL.
		StringRequest stringRequest = new StringRequest(Request.Method.GET,
				url, new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						// Display the first 500 characters of the response
						// string.
						if (Url.contains("lists.delete.php")) {
							toast("List deleted");
						}
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
					}
				});
		// Add the request to the RequestQueue.
		queue.add(stringRequest);
	}

	private void toast(String str) {
		Toast.makeText(mContext, str, Toast.LENGTH_LONG).show();
	}
}
