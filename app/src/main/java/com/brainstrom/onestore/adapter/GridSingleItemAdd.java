package com.brainstrom.onestore.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.brainstrom.onestore.R;
import com.brainstrom.onestore.database.ShoppingListDatabaseHandler;
import com.brainstrom.onestore.database.ShoppingListFrame;
import com.loopj.android.image.SmartImageView;

public class GridSingleItemAdd extends BaseAdapter {
	ArrayList<singleItemrow> list;
	ArrayList<String> aListId = new ArrayList<String>();
	ArrayList<String> aListName = new ArrayList<String>();
	ArrayList<String> aListPrice = new ArrayList<String>();

	Context context;

	public GridSingleItemAdd(Context c, ArrayList<String> arraylistImage,
			ArrayList<String> arraylistId, ArrayList<String> arraylistName,
			ArrayList<String> arraylistCost) {
		context = c;
		list = new ArrayList<singleItemrow>();

		for (int i = 0; i < arraylistImage.size(); i++) {
			list.add(new singleItemrow(arraylistImage.get(i), arraylistName
					.get(i) + " (" + arraylistCost.get(i) + ")"));
		}

		aListId = arraylistId;
		aListName = arraylistName;
		aListPrice = arraylistCost;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return list.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(final int arg0, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		LayoutInflater inflator = (LayoutInflater) context
				.getSystemService(context.LAYOUT_INFLATER_SERVICE);
		View row = inflator.inflate(R.layout.single_row_item_add, arg2, false);
		TextView title = (TextView) row.findViewById(R.id.tvSingleGridItem);
		SmartImageView img = (SmartImageView) row
				.findViewById(R.id.ivSingleGridItem);
		TextView add = (TextView) row.findViewById(R.id.tvSingleGridButtonAdd);
		
		add.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				addToShopping(arg0);
			}
		});

		singleItemrow temp = list.get(arg0);
		title.setText(temp.title);
		img.setImageUrl(temp.image);
		return row;
	}

	private void toast(String str) {
		Toast.makeText(context, str, Toast.LENGTH_SHORT).show();
	}

	private void addToShopping(int pos) {
		try {
			int id = Integer.parseInt(aListId.get(pos));
			ShoppingListDatabaseHandler db = new ShoppingListDatabaseHandler(
					context);
			boolean has = db.Exists(id);
			if (has) {
				ShoppingListFrame sl = db.getshoppingList(id);
				db.updateshoppingList(new ShoppingListFrame(id, aListName
						.get(pos), aListPrice.get(pos), sl.getQuantity() + 1));

				Log.d("added2", "Id : " + id + " Name : " + aListName.get(pos)
						+ " Cost : " + aListPrice.get(pos) + " Quantity : "
						+ sl.getQuantity() + 1);
			} else {
				db.addshoppingList(new ShoppingListFrame(id,
						aListName.get(pos), aListPrice.get(pos), 1));
				Log.d("added3", "Id : " + id + " Name : " + aListName.get(pos)
						+ " Cost : " + aListPrice.get(pos) + " Quantity : " + 1);
			}
			toast(aListName.get(pos) + " Added to shopping list");
		} catch (Exception e) {
			toast("Error  : " + e.getMessage());
		}
	}
}

class singleItemrow {
	String title, image;

	singleItemrow(String image, String title) {
		this.title = title;
		this.image = image;
	}
}
